export type ModelFloorMapDictionary = {
    [key: string]: string;
}

export type ModelPackageData = {
    floorMaps: ModelFloorMapDictionary
}