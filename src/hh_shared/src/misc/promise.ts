export type StatusPromise<T> = Promise<T> & PromiseUtils.CompletionStatus<T>;

export namespace PromiseUtils {
    export const delay = (duration: number) => {
        return new Promise(function (resolve, reject) {
            setTimeout(() => {
                resolve();
            }, duration)
        });
    }

    export type CompletionStatus<T> = { isPending: boolean, isFulfilled: boolean, isRejected: boolean, value?: T};

    export function wrap<V>(oldPromise: Promise<V>): StatusPromise<V> {
        const result: Partial<StatusPromise<V>> = oldPromise;

        result.isPending = true,
        result.isRejected = false;
        result.isFulfilled = false;
        result.value = null;

        oldPromise.then((val) => {
            result.isPending = false;
            result.isFulfilled = true;
            result.value = val;
            return val;
        }, (e) => {
            result.isPending = false;
            result.isRejected = true;
            result.value = null;
            return e;
        });

        return result as StatusPromise<V>;
    }
}
