export type ColorRGB = { r: number, g: number, b: number };
export type ColorHex = number;
export type ColorString = string;

export namespace ColorUtils {
    export const rgb2hex = (rgb: ColorRGB): ColorHex => {
        return (((rgb.r * 255) << 16) + ((rgb.g * 255) << 8) + (rgb.b * 255 | 0));
    }

    export const hex2rgb = (hex: ColorHex): ColorRGB => {
        const r = ((hex >> 16) & 0xFF) / 255;
        const g = ((hex >> 8) & 0xFF) / 255;
        const b = (hex & 0xFF) / 255;
        return { r, g, b };
    }

    export const hex2rawRgb = (hex: number, out: Array<number>= []): Array<number> => {
        out[0] = ((hex >> 16) & 0xFF) / 255;
        out[1] = ((hex >> 8) & 0xFF) / 255;
        out[2] = (hex & 0xFF) / 255;

        return out;
    }

    export const string2hex = (strIn: ColorString): ColorHex => {
        if (strIn[0] === "#") {
            strIn = strIn.substr(1);
        }
        return parseInt(strIn, 16);
    }

    export const hex2string = (hex: ColorHex): ColorString => {
        let hexString = hex.toString(16);
        hexString = "000000".substr(0, 6 - hexString.length) + hexString;
        return `#${hexString}`;
    }
}