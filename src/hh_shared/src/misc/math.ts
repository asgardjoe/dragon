export type Dimensions = {
    width: number,
    height: number
}

export namespace Dimensions {
    export const of = (width: number, height: number): Dimensions => {
        return {width, height};
    }
}

export type Point = {
    x: number,
    y: number
}

export namespace Point {
    export const of = (x: number, y: number): Point => {
        return {x, y};
    }
}

export type IsometricDimensions = {
    x: number,
    y: number,
    z: number
}

export namespace IsometricDimensions {
    export const of = (x: number, y: number, z: number): IsometricDimensions => {
        return {x, y, z};
    }
}

export type IsometricPoint = {
    x: number,
    y: number,
    z: number
}

export namespace IsometricPoint {
    export const of = (x: number, y: number, z: number): IsometricDimensions => {
        return {x, y, z};
    }

    export const from = (iso: {x: number, y: number, z: number}): IsometricDimensions => {
        return iso;
    }
}
