import { Point } from "../misc/math";

export class ButtonModifiers {
    ctrlKey: boolean;
    shiftKey: boolean;
    altKey: boolean;

    constructor(ctrlKey: boolean, shiftKey: boolean, altKey: boolean) {
        this.ctrlKey = ctrlKey;
        this.shiftKey = shiftKey;
        this.altKey = altKey;
    }
}

export interface InputEvent {
    position: Point;
    buttonModifiers: ButtonModifiers;
}

export class MouseMoveEvent implements InputEvent {
    position: Point;
    buttonModifiers: ButtonModifiers;
    isDrag: boolean

    constructor(position: Point, buttonModifiers: ButtonModifiers, isDrag: boolean) {
        this.position = position;
        this.buttonModifiers = buttonModifiers;
        this.isDrag = isDrag;
    }
}

export class MouseClickEvent implements InputEvent {
    position: Point;
    buttonModifiers: ButtonModifiers;

    constructor(position: Point, buttonModifiers: ButtonModifiers) {
        this.position = position;
        this.buttonModifiers = buttonModifiers;
    }
}

export class MouseDoubleClickEvent implements InputEvent {
    position: Point;
    buttonModifiers: ButtonModifiers;

    constructor(position: Point, buttonModifiers: ButtonModifiers) {
        this.position = position;
        this.buttonModifiers = buttonModifiers;
    }
}