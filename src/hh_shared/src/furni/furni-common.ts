export type FurniNameColorPair = [string, number];

export type FurniDirection = 0 | 2 | 4 | 6;

export namespace FurniCommon {
    export const splitNameAndColor = (asset: string): FurniNameColorPair => {
        let colorId = 0;
        let itemName = asset;
        if (asset.includes("*")) {
            const splitClassName = asset.split("*");
            itemName = splitClassName[0];
            colorId = parseInt(splitClassName[1]);
        }
        return [itemName, colorId];
    };
}