import fetch from "cross-fetch";
import JSZip from "jszip";

export enum AssetType {
    UNKNOWN,
    IMAGE,
    TEXT
};

export type AssetPackage = {
    [key: string]: AssetPackageEntry;
}

export type AssetPackageEntry = {
    rawName: string,
    trimmedName: string,
    type: AssetType,
    content: string | ArrayBuffer
}

export namespace PackageCommon {

}

export namespace FetchCommon {
    export const fetchBinary = async (url: string): Promise<ArrayBuffer> => {
        const request = await fetch(url);
        if (request.ok) {
            const body = await request.arrayBuffer();
            return body;
        } else {
            const error = await request.text();
            throw Error("Could not retrieve") + ": " + error;
        }
    }

    export const fetchText = async (url: string): Promise<string> => {
        const request = await fetch(url);
        if (request.ok) {
            const body = await request.text();
            return body;
        } else {
            const error = await request.text();
            throw Error("Could not retrieve") + ": " + error;
        }
    }

    export async function fetchJSON<T>(url: string): Promise<T> {
        const content = await fetchText(url);
        const result = <T>JSON.parse(content);
        return result;
    }

    export const fetchZip = async (url: string): Promise<JSZip> => {
        const content = await fetchBinary(url);
        const zip = await JSZip.loadAsync(content);
        return zip;
    }

    export const fetchPackage = async (url: string): Promise<AssetPackage> => {
        const result: AssetPackage = {};

        const zip = await fetchZip(url);
        for (const fileName in zip.files) {
            const file = zip.files[fileName];
            let type;
            let extension;
            let content;

            if (fileName.endsWith(".png")) {
                type = AssetType.IMAGE;
                extension = ".png"
            } else if (fileName.endsWith(".json")) {
                type = AssetType.TEXT
                extension = ".json"
            } else if (fileName.endsWith(".txt")) {
                type = AssetType.TEXT
                extension = ".txt"

            } else {
                type = AssetType.UNKNOWN
            }

            switch (type) {
                case AssetType.TEXT:
                    content = await file.async("string");
                    break;
                case AssetType.IMAGE:
                default:
                    content = await file.async("arraybuffer");
                    break;
            }

            const trimmedName = fileName.substring(0, fileName.length - extension.length);

            const entry: AssetPackageEntry = {
                rawName: fileName,
                trimmedName: trimmedName,
                type: type,
                content: content
            }

            result[trimmedName] = entry;
        }

        return result;
    }
}
