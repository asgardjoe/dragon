export type HumanAnimationPartEntry = {
    number: number;
    part: string;
    repeat: number;
};

export type HumanAnimationPartTypeDictionary = {
    [key: string]: HumanAnimationPartEntry[];
};

export type HumanAnimation = {
    part: HumanAnimationPartTypeDictionary;
};

export type HumanAnimations = {
    [key: string]: HumanAnimation;
};