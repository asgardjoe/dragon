export type HumanGender = "M" | "F" | "U";
export type HumanDirection = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;

export type HumanAssetDictionary = {
    [key: string]: HumanAsset;
};

export type HumanAsset = {
    name: string;
    offsetX: number;
    offsetY: number;
    exists: boolean;
    flipH: boolean;
    flipV: boolean;
    source?: string;
};

export type HumanDrawOrder = {
    [key: string]: {
        [key in HumanDirection]: string[];
    };
};