import { HumanGender } from "./human-common";

export type FigureDataColor = {
    index: number;
    id: number;
    club: number;
    selectable: boolean;
    colorCode: string;
};
export type FigureDataColorsDictionary = {
    [key: number]: FigureDataColor;
};
export type FigureDataPaletteDictionary = {
    [key: number]: FigureDataPalette;
};
export type FigureDataPalette = {
    id: number;
    color: FigureDataColorsDictionary;
};
export type FigureDataPart = {
    id: number;
    index: number;
    type: string;
    colorable: boolean;
    colorIndex: number;
};
export type FigureDataSet = {
    id: number;
    gender: HumanGender;
    club: number;
    colorable: boolean;
    selectable: boolean;
    preselectable: boolean;
    part: FigureDataPart[];
    hidden: string[];
};
export type FigureDataSetDictionary = {
    [key: number]: FigureDataSet;
};
export type FigureDataSetType = {
    type: string;
    paletteId: number;
    mand_m_0: boolean;
    mand_m_1: boolean;
    mand_f_0: boolean;
    mand_f_1: boolean;
    set: FigureDataSetDictionary;
};
export type FigureDataSetTypesDictionary = {
    [key: string]: FigureDataSetType;
};
export type FigureData = {
    colors: FigureDataPaletteDictionary;
    sets: FigureDataSetTypesDictionary;
};

export type FigureMapping = {
    [key: number]: string[];
};
export type FigureMapDictionary = {
    [key: string]: FigureMapping;
};