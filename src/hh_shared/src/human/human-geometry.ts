export type HumanGeometryCanvas = {
    width: number;
    height: number;
    dx: number;
    dy: number;
};

export type HumanGeometryCanvases = {
    [key: string]: HumanGeometryCanvas;
};

export type HumanGeometry = {
    canvas: {
        [key: string]: HumanGeometryCanvases;
    };
};