export type HumanPartSet = {
    "flipped-set-type": string,
    "remove-set-type": string,
    swim: boolean
}

export type HumanPartSetDictionary = {
    [key: string]: HumanPartSet
}

export type HumanActivePartSet = {
    activePart: [string]
}

export type HumanActivePartSetDictionary = {
    [key: string]: HumanActivePartSet
}

export type HumanPartSets = {
    partSet: HumanPartSetDictionary,
    activePartSet: HumanActivePartSetDictionary
};