export namespace ConvertUtils {
    export const convertBufferToImage = (buffer: Buffer | ArrayBuffer): HTMLImageElement => {
        buffer = (buffer instanceof ArrayBuffer) ? Buffer.from(buffer) : buffer;
        const image = new Image();
        image.src = "data:image/png;base64," + buffer.toString("base64");
        return image;
    }
}