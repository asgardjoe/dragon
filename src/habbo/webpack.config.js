const HtmlWebpackPlugin = require("html-webpack-plugin");
var WebpackAutoInject = require("webpack-auto-inject-version");
const CopyPlugin = require('copy-webpack-plugin');

const path = require("path");

const resourcesPath = path.join(__dirname, ".." , "..", "resources", "public");

module.exports = {
    mode: "production",

    devServer: {
        port: 8980,
        contentBase: [
            resourcesPath,
            "../../resources",
            "../../webtmp"
        ]
    },

    output: {
        filename: "client.js"
    },

    devtool: "source-map",

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"]
    },

    performance: {
        hints: false
    },

    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "ts-loader"
                    }
                ]
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ["file-loader", "image-webpack-loader"],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    "file-loader",
                ]
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/template.html"
        }),
        new CopyPlugin([
            { from: resourcesPath, to: "." }
        ]),
        new WebpackAutoInject({
            SILENT: true,
            SHORT: "FERMI-BUILD",

            components: {
                AutoIncreaseVersion: false,
                InjectAsComment: true,
                InjectByTag: true
            },

            componentsOptions: {
                InjectAsComment: {
                    tag: "{version} ({date})"
                }
            }
        }),
    ],
};