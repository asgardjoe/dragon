import React, { FormEvent, useRef, useState, useEffect } from "react";

import "./login-window.css";
import { WindowComponent } from "../../components/window/window-component";
import { Game } from "hh_game";
import { AlertManager } from "../alert/alert-manager";
import { UIManager } from "../../ui-manager";

export const LoginWindow = () => {
    const username = useRef<HTMLInputElement>();
    const password = useRef<HTMLInputElement>();
    const [loggingIn, setLoggingIn] = useState(false);

    useEffect(() => {
        Game.instance.events.on("LoginStart", () => {
            setLoggingIn(true);
        });
    
        Game.instance.events.on("LoginFailed", () => {
            setLoggingIn(false);
            password.current.focus();
            UIManager.instance.alerts.fireAlert("Error", "Incorrect login details.");
        });
    }, []);

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        Game.instance.handshake.attemptLogin(
            username.current.value,
            password.current.value
        );
        e.preventDefault();
    };

    return (
        <WindowComponent
            className="loginWindow"
            title="What's your Habbo called?"
            titleFont="Volter-Bold"
            width={250}
            customStyle={{
                textAlign: "center",
                fontSize: "9pt",
                color: "white",
                position: "absolute",
                right: 100,
                top: 300
            }}
        >
            <WindowComponent
                backgroundColor="white"
                borderBlur={0}
                borderSpread={0}
                customStyle={{
                    textAlign: "center",
                    fontSize: "9pt",
                    color: "black",
                    display: "inline-block"
                }}
            >
                <form onSubmit={onSubmit}>
                    <p>Name of your Habbo</p>
                    <input
                        disabled={loggingIn}
                        type="text"
                        name="loginUsername"
                        ref={username}
                        autoFocus={true} style={{
                            width: 210,
                            height: 40,
                            fontSize: "14pt"
                        }}
                    />
                    <p>Password</p>
                    <input
                        disabled={loggingIn}
                        type="password"
                        name="loginPassword"
                        ref={password}
                        style={{
                            width: 210,
                            height: 40,
                            fontSize: "14pt"
                        }}
                    />
                    <p></p>
                    <button disabled={loggingIn} type="submit">OK</button>
                </form>
            </WindowComponent>

            <p style={{
                color: "black",
                textDecoration: "underline"
            }}>
                Forgotten your password?
            </p>
        </WindowComponent>
    );
};