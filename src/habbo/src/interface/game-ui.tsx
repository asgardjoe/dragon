import React, { useState, useEffect, useCallback } from "react";
import { ErrorBoundary } from "./misc/error-boundary";
import { LoadingScreen } from "./screens/loading/loading-screen";
import { Game } from "hh_game";

import "./game-ui.css";
import { LoginScreen } from "./screens/login/login-screen";
import { BetaLogoComponent } from "./components/beta-logo/beta-logo-component";
import { HotelViewScreen } from "./screens/hotel-view/hotel-view-screen";
import { UIManager } from "./ui-manager";
import { RoomScreen } from "./screens/room/room-screen";


type GameInterfaceProps = {
    game: Game
}

export const GameUI = ({ game }: GameInterfaceProps) => {
    const [screen, setScreen] = useState(<></>);
    const [alerts, setAlerts] = useState([]);

    useEffect(() => {
        game.events.on("GameLoading", () => {
            setScreen(<LoadingScreen></LoadingScreen>);
        });

        game.events.on("GameLoaded", () => {
            setScreen(<LoginScreen></LoginScreen>);
        });

        game.events.on("LoginSuccess", () => {
            setScreen(<HotelViewScreen></HotelViewScreen>);
        });

        game.events.on("RoomLeft", () => {
            setScreen(<HotelViewScreen></HotelViewScreen>);
        });

        game.events.on("RoomLoading", () => {
            setScreen(<></>);
        });

        game.events.on("RoomEntered", () => {
            setScreen(<RoomScreen></RoomScreen>);
        });
    }, []);

    UIManager.instance.alerts.init((alerts) => {
        setAlerts(alerts);
    });

    return (
        <ErrorBoundary>
            <BetaLogoComponent />
            {screen}
            {alerts}
        </ErrorBoundary>
    );
}