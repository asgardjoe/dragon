import { AlertManager } from "./windows/alert/alert-manager";
import React from "react";
import { GameUI } from "./game-ui";
import { Game } from "hh_game";
import ReactDOM from "react-dom";

export class UIManager {
    private _gameUi: JSX.Element;
    private _alerts = new AlertManager();

    init(domElement: HTMLElement) {
        const ui = <GameUI game={Game.instance}></GameUI>;
        this._gameUi = ui;
        ReactDOM.render(ui, domElement);
    }

    get alerts() {
        return this._alerts;
    }

    private static _instance: UIManager;
    static get instance() {
        if (!UIManager._instance) UIManager._instance = new UIManager();
        return UIManager._instance;
    }
}