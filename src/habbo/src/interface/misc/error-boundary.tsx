import React from "react"

import MaintenanceLogo from "../../../../../resources/interface/misc/maintenance_logo.png";
import { Game } from "hh_game";

type ErrorBoundaryState = {
    hasError: boolean
}

export class ErrorBoundary extends React.Component<{}, ErrorBoundaryState> {
    triggerError() {
        this.setState({ hasError: true }); 
        Game.instance.events.emit("FatalError");
     }

    componentWillMount() {
        this.setState({ hasError: false });

        window.onerror = (message, file, line, col, error) => {
            this.triggerError();
            return false;
        };
        window.addEventListener("error", (e) => {
            this.triggerError();
            return false;
        });

        window.addEventListener("unhandledrejection", (e) => {
            this.triggerError();
        })
    }

    componentDidCatch() {
        this.triggerError();
    }

    render() {
        const { children } = this.props;

        if (this.state.hasError) {
            return (
                <div className="centerOnScreen">
                    <div>
                        <img src={MaintenanceLogo} />
                        <br />
                        <p style={{ color: "white" }}>A fatal error has occurred!</p>
                    </div>
                </div>);
        } else {
            return children;
        }
    }
}