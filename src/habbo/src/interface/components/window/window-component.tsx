import React, { FunctionComponent } from "react";
import Draggable from "react-draggable";

import "./window-component.css"

type WindowComponentProps = {
    title?: string,
    titleFont?: string
    backgroundColor?: string,
    borderColor?: string,
    borderRadius?: number,
    borderBlur?: number,
    borderSpread?: number,
    width?: number,
    height?: number,
    customStyle?: any,
    draggable?: boolean,
    className?: string,
    [propName: string]: {}
}

export const WindowComponent: FunctionComponent<WindowComponentProps> = ({
    title = "",
    titleFont,
    backgroundColor = "#6794A7",
    borderColor = "black",
    borderRadius = 10,
    borderBlur = 4,
    borderSpread = 10,
    customStyle = {},
    draggable = true,
    className = "",
    width,
    height,
    children,
    ...rest
}) => {
    const windowStyle: any = {
        paddingLeft: `${borderRadius}px`,
        paddingRight: `${borderRadius}px`,
        borderRadius: `${borderRadius}px`,
        boxShadow: `0px 0px 0px 1px ${borderColor}, ${borderBlur}px ${borderBlur}px ${borderSpread}px 0px ${borderColor}`,
        backgroundColor,
        width,
        height
    }

    const generatedStyle = Object.assign({}, windowStyle, customStyle);

    const content = (
        <div className={"windowComponent " +  className} style={generatedStyle} {...rest} >
            <div className="windowDragHandle">
                {title ? <p className="title" style={{ fontFamily: titleFont }}>{title}</p> : undefined}
            </div>
            {children}
        </div>
    );

    const final = draggable ? <Draggable handle=".windowDragHandle">{content}</Draggable> : content;

    return final;
}