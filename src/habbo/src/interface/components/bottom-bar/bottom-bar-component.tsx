import React, { FunctionComponent, useEffect, useState } from "react";
import useInterval from "use-interval";
import { Game } from "hh_game";

import "./bottom-bar-component.css";
import GhostHeadIcon from "../../../../../../resources/interface/icons/ghost_head.png";
import NavIcon from "../../../../../../resources/interface/icons/navigator_icon.png";

export const BottomBarComponent = () => {
    const [headSrc, setHeadSrc] = useState(GhostHeadIcon);

    const onToggleNav = () => {
        Game.instance.room.tryRoom("1000");
    }

    useInterval(() => {
        const tmpSrc = Game.instance.user.headSrc;
        if (tmpSrc) {
            setHeadSrc(tmpSrc);
        }
    }, 100);

    return (
        <div className="bottomBar">
            <img className="icon" src={headSrc} />
            <img className="icon" src={NavIcon} onClick={onToggleNav} />
            <img className="icon" src={NavIcon} />
        </div>
    );
}