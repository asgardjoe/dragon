import React from "react";
import Draggable from "react-draggable";

import BetaLogoImage from "../../../../../../resources/interface/hotel_view/beta_logo.png";

import "./beta-logo-component.css";

export const BetaLogoComponent = () => {
    return (
        <Draggable defaultClassName="betaLogo" defaultPosition={{
            x: 10,
            y: 10
        }}>
            <img draggable="false" src={BetaLogoImage} />
        </Draggable>
    )
}