import React from "react";
import "./room-screen.css";
import { BottomBarComponent } from "../../components/bottom-bar/bottom-bar-component";

export const RoomScreen = () => {
    return (
        <div className="gameScreen roomScreen">
            <BottomBarComponent></BottomBarComponent>
        </div>
    );
}