import React from "react";

import "./login-screen.css";
import { LoginWindow } from "../../windows/login/login-window";

export const LoginScreen = () => {
    return (
        <div className="gameScreen loginScreen">
            <LoginWindow />
        </div>
    );
}