import React from "react";

import "./loading-screen.css";

import Logo from "../../../../../../resources/interface/loading/sulake_logo.png";
import LoadingBar from  "../../../../../../resources/interface/loading/loading_bar.gif";

export const LoadingScreen = () => {
    return (
        <div className="centerOnScreen">
            <div>
                <img src={Logo} />
                <br />
                <img src={LoadingBar} />
            </div>
        </div>
    );
}