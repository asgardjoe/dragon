import React from "react";
import ReactDOM from "react-dom";

import "reset-css";
import "../../../resources/interface/fonts/fonts.css";
import "./index.css";
import { Game } from "hh_game";
import { UIManager } from "./interface/ui-manager";
import { InputHandler } from "./input/input-handler";

export const GAME_BUILD_VERSION = "[AIV]{version}[/AIV]";
export const GAME_BUILD_DATE = "[AIV]{date}[/AIV]";

const startDragon = () => {
    console.log(`%c 🐲 Dragon | Build: ${GAME_BUILD_VERSION} (${GAME_BUILD_DATE}) 🐲`, "background: #222; color: #bada55");

    const uiRootElement = document.getElementById("uiRoot");
    const renderRootElement = document.getElementById("renderRoot");

    // Input system
    const inputHandler = new InputHandler();
    inputHandler.init(renderRootElement, (event) => {
        Game.instance.onInput(event);
    });

    // Initialize game engine
    Game.instance.init(renderRootElement);

    // Init UI
    UIManager.instance.init(uiRootElement);

    // Start game engune
    Game.instance.start();
}

startDragon();