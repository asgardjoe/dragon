import { ServerMessage, ClientMessages } from "hh_fuseclient";
import { Game } from "../game";

export class HandshakeSystem {
    /* 
    1. [C->S] OPEN CONNNECTION
    2. [S->C] HELLO
    3. [C->S] LOGIN
    4. [S->C] LOGIN_OK or ERROR
    */

    private _creds: {username: string, password: string};

    start() {
        Game.instance.client.registerListener(ServerMessage.HELLO, (_) => this.handleHello());
        Game.instance.client.registerListener(ServerMessage.LOGIN_OK, (_) => this.handleLoginOk());
    }

    // 1
    attemptLogin(username: string, password: string) {
        this._creds = { username, password };

        Game.instance.events.emit("LoginStart");
        Game.instance.client.connect();
    }

    //2
    private handleHello() { 
        // 3
        Game.instance.client.send(
            new ClientMessages.TryLoginMessage(this._creds.username, this._creds.password)
        );
    }

    //4
    private handleLoginOk() { 
        Game.instance.events.emit("LoginSuccess");
       Game.instance.user.onUserLoggedIn();
    }

    // 4
    loginFailed() {
        Game.instance.client.disconnect();
        Game.instance.events.emit("LoginFailed");
    }


}