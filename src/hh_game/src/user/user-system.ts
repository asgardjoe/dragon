import { ServerMessage, ClientMessages, ServerMessages } from "hh_fuseclient";
import { Game } from "../game";
import { HumanRenderer } from "../human/human-renderer";
import { WorldSize } from "hh_shared";

export type UserObject = {
    id: string;
    name: string;
    figure: string;
    gender: string;
    motto: string;
    tickets: number;
    poolFigure: string;
    film: number;
}

export class UserSystem {
    private _isLoggedIn = false;
    private _userObject?: UserObject;
    private _fuseRights: string[] = [];
    private _credits = 0;
    private _availableSets: number[] = [];
    private _badge: {
        visible: boolean,
        selectedIndex: number,
        badges: string[]
    };

    private _userHeadRenderer: HumanRenderer;
    private _userHeadSrc: string;

    start() {
        Game.instance.client.registerListener(ServerMessage.FUSE_RIGHTS, (msg) => this.handleFuseRights(msg));
        Game.instance.client.registerListener(ServerMessage.USER_OBJECT, (msg) => this.handleUserObject(msg));
        Game.instance.client.registerListener(ServerMessage.CREDIT_BALANCE, (msg) => this.handleCreditBalance(msg));
        Game.instance.client.registerListener(ServerMessage.AVAILABLE_SETS, (msg) => this.handleAvailableSets(msg));
        Game.instance.client.registerListener(ServerMessage.AVAILABLE_BADGES, (msg) => this.handleAvailableBadges(msg));
    }

    update(delta: number) {

        if(this._userHeadRenderer) {
            this._userHeadRenderer.update(delta);
            if(this._userHeadRenderer.isGhost) {
                this._userHeadSrc = undefined;
            } else {
                const canvas = Game.instance.renderer.visualRenderer.extract.canvas(this._userHeadRenderer.drawResult.visualContainer);
                this._userHeadSrc = canvas.toDataURL();
            } 
        }         
    }

    onUserLoggedIn() {
        this._isLoggedIn = true;
        Game.instance.client.send(new ClientMessages.GetInfo());
        Game.instance.client.send(new ClientMessages.GetCredits());
        Game.instance.client.send(new ClientMessages.GetAvailableSets());
        Game.instance.client.send(new ClientMessages.GetAvailableBadges());
    }

    private handleFuseRights(msg: ServerMessages.FuseRights) {
        this._fuseRights = msg.rights;
    }

    private handleUserObject(msg: ServerMessages.UserObject) {
        this._userObject = {
            id: msg.id,
            name: msg.name,
            figure: msg.figure,
            gender: msg.gender,
            motto: msg.motto,
            tickets: msg.tickets,
            poolFigure: msg.poolFigure,
            film: msg.film
        };

        this._userHeadRenderer = new HumanRenderer({worldSize: WorldSize.LARGE, figure: msg.figure}, {headOnly: true, headDirection:2})
    }

    private handleCreditBalance(msg: ServerMessages.CreditBalance) {
        this._credits = msg.credits;
    }

    private handleAvailableSets(msg: ServerMessages.AvailableSets) {
        this._availableSets = msg.sets;
    }

    private handleAvailableBadges(msg: ServerMessages.AvailableBadges) {
        this._badge = {
            badges: msg.badges,
            selectedIndex: msg.selectedIndex,
            visible: msg.visible

        };
    }

    get headSrc() {
        return this._userHeadSrc;
    }
}