import * as PIXI from "pixi.js";
import { Dimensions } from "hh_shared";

export namespace TileGraphics {
    export const generateFloorTile = (tileDimensions: Dimensions, tileThickness: number, tileColor: number): PIXI.Graphics => {
        const graphics = new PIXI.Graphics();

        const startX = tileDimensions.height;
        const startY = 0;

        const points = [
            {
                x: startX,
                y: startY
            },
            {
                x: startX - tileDimensions.width / 2,
                y: startY + tileDimensions.height / 2
            },
            {
                x: startX,
                y: startY + tileDimensions.height
            },
            {
                x: startX + tileDimensions.width / 2,
                y: startY + tileDimensions.height / 2
            }
        ];

        graphics.beginFill(0xFFFFFF)
            .lineStyle(1, 0xD3D3D3)
            .moveTo(points[0].x, points[0].y)
            .lineTo(points[1].x, points[1].y)
            .lineTo(points[2].x, points[2].y)
            .lineTo(points[3].x, points[3].y)
            .lineTo(points[0].x, points[0].y)
            .endFill();


        if (tileThickness > 0) {

            graphics.beginFill(0xC0C0C0)
                .lineStyle(1, 0xA9A9A9)
                .moveTo(points[1].x - 0.5, points[1].y)
                .lineTo(points[1].x - 0.5, points[1].y + tileThickness)
                .lineTo(points[2].x - 0.5, points[2].y + tileThickness)
                .lineTo(points[2].x - 0.5, points[2].y)
                .endFill();

            graphics.beginFill(0x808080)
                .lineStyle(1, 0x696969)
                .moveTo(points[3].x + 0.5, points[3].y)
                .lineTo(points[3].x + 0.5, points[3].y + tileThickness)
                .lineTo(points[2].x + 0.5, points[2].y + tileThickness)
                .lineTo(points[2].x + 0.5, points[2].y)
                .endFill();
        }

        graphics.tint = tileColor;

        return graphics;
    }

    export const generateHighlightTile = (tileDimensions: Dimensions, highlightColor: number, highlightThickness: number): PIXI.Graphics => {
        const graphics = new PIXI.Graphics();
        
        const startX = tileDimensions.height;
        const startY = 0;

        const points = [
            {
                x: startX,
                y: startY
            },
            {
                x: startX - tileDimensions.width / 2,
                y: startY + tileDimensions.height / 2
            },
            {
                x: startX,
                y: startY + tileDimensions.height
            },
            {
                x: startX + tileDimensions.width / 2,
                y: startY + tileDimensions.height / 2
            }
        ];

        graphics.lineStyle(highlightThickness, 0xFFFFFF)
            .moveTo(points[0].x, points[0].y)
            .lineTo(points[1].x, points[1].y)
            .lineTo(points[2].x, points[2].y)
            .lineTo(points[3].x, points[3].y)
            .lineTo(points[0].x, points[0].y);

            graphics.tint = highlightColor;

        return graphics;
    }
}