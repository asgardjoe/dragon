import { FetchCommon, ModelPackageData } from "hh_shared";

export class ModelLoader { 
    async loadModelPackage(roomType: string): Promise<ModelPackageData> {
        const assetPackage = await FetchCommon.fetchPackage(`/model/packages/${roomType}.zip`);
        const roomData = <ModelPackageData> JSON.parse(<string>assetPackage["model"].content);
        return roomData;
    }
}