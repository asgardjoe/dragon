import { Dimensions } from "hh_shared";

export namespace ModelHelpers {
    export const textMapToArray = (mapped: string, splitter = "|"): number[][] => {
        const lines = mapped.split(splitter);
        const mapX = lines[0].length;
        const mapY = lines.length;

        const result: number[][] = new Array(mapX);

        for (let y = 0; y < mapY; y++) {
            let line = lines[y];
            for (let x = 0; x < mapX; x++) {
                if(!result[x]) result[x] = new Array(mapY);

                const tile = line[x];
                if(tile.toLowerCase() === "x") {
                    result[x][y] = -1;
                } else {
                    result[x][y] = parseInt(tile);
                }
            }
        }

        return result;
    }

    export const calculateDimensions = (map: number[][]): Dimensions => {
        return {
            width: map.length - 1,
            height: map[0].length - 1
        }
    }
}