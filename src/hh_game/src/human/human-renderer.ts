import * as PIXI from "pixi.js";
import { WorldSize, HumanDirection, Dimensions, ColorUtils, Point } from "hh_shared";
import { HumanManager } from "./human-manager";
import { HumanLoader, LoadedHumanAsset } from "./human-loader";
import { SolidColorFilter } from "../renderer/render-filters";
import { HumanHelpers, HumanPart } from "./human-helpers";
import { Game } from "../game";


const FRAME_SPEED = 120;
const MAX_LAYERS = 50;
const LOADING_FILTERS = [new SolidColorFilter(0xFFFFFF), new PIXI.filters.AlphaFilter(0.8)];

export type HumanDrawResult = { logicContainer: PIXI.Container, visualContainer: PIXI.Container }

export type HumanRendererOpts = {
    worldSize: WorldSize,
    figure: string
};

export type HumanState = {
    direction?: HumanDirection,
    headDirection?: HumanDirection,
    handItem?: number,
    gesture?: string,
    actions?: HumanActions,
    headOnly?: boolean
};

export type HumanActionParams = string[];
export type HumanActions = { [key: string]: HumanActionParams };

export type HumanDrawAction = {
    geometry: string,
    body: string,
    wlk?: string,
    sit?: string,
    gesture?: string,
    eye?: string,
    speak?: string,
    itemRight?: string,
    handRight?: string,
    handLeft?: string,
    swm?: string,
    shadow?: string
};

export class HumanRenderer {
    protected _worldSize: WorldSize;
    protected _logicColor: number;

    private _humanManager: HumanManager;
    private _humanLoader: HumanLoader;
    private _frameCounter: number = 0;
    private _totalFrames: number = 0;

    private _visualContainer: PIXI.Container;
    private _visualSprites: PIXI.Sprite[];
    private _logicContainer: PIXI.Container;
    private _logicSprites: PIXI.Sprite[];

    private _drawResult: HumanDrawResult;
    private _drawOrder: string = "std";

    private _figure: HumanPart[];

    private _direction: HumanDirection = 4;
    private _headDirection: HumanDirection = 4;
    private _handItem: number = -1;
    private _gesture: string = "";
    private _actions: HumanActions = {};
    private _headOnly: boolean = false;

    private _isGhost = true;

    constructor(opts: HumanRendererOpts, initialState: HumanState) {
        this._humanManager = Game.instance.humans;
        this._humanLoader = this._humanManager.loader;
        this._worldSize = opts.worldSize;
        this._logicColor =  Math.floor(Math.random() * (16777215 - 1)) + 1;
        this.generateLayers();
        this.setState(initialState)
        this._figure = HumanHelpers.extractFigureParts(opts.figure);

    }

    update(delta: number) {
        this._frameCounter += delta;
        if (this._frameCounter >= FRAME_SPEED) {
            this._totalFrames++;
            this._frameCounter = 0;
            this.drawNextFrame();
        }
    }

    setState(state: HumanState) {
        this._direction = state.direction ?? this._direction;
        this._headDirection = state.headDirection ?? this._headDirection;
        this._handItem = state.handItem ?? this._handItem;
        this._gesture = state.gesture ?? this._gesture;
        this._actions = state.actions ?? this._actions;
        this._headOnly = state.headOnly ?? this._headOnly;
    }

    protected drawNextFrame() {
        this.logic();
    }

    private logic() {
        const drawActions: HumanDrawAction = { body: "std", shadow: "std", geometry: "vertical" };

        switch (this._gesture) {
            case "spk":
                drawActions.speak = "spk";
                break;
            case "eyb":
                drawActions.eye = "eyb";
                break;
            case "":
                drawActions.gesture = "std";
                break;
            default:
                drawActions.gesture = this._gesture;
                break;
        }

        drawActions.geometry = "vertical";
        drawActions.gesture = "std";

        for (const actionName in this._actions) {
            const actionParams = this._actions[actionName];
            switch (actionName) {
                case "wlk":
                    drawActions.wlk = "wlk";
                    drawActions.geometry = "vertical";
                    break;
                case "sit":
                    drawActions.sit = "sit";
                    drawActions.geometry = "sitting";
                    drawActions.shadow = "sit";

                    break;
                case "lay":
                    drawActions.body = "lay";
                    drawActions.eye = "lay";
                    drawActions.geometry = "horizontal";


                    switch (drawActions.gesture) {
                        case "spk":
                            drawActions.speak = "lsp";
                            break;

                        case "eyb":
                            drawActions.eye = "ley";
                            break;

                        case "std":
                            drawActions.gesture = "lay";
                            break;

                        default:
                            drawActions.gesture = "l" + this._gesture.substr(0, 2);
                            break;
                    }
                    break;
                case "wav":
                    drawActions.handLeft = "wav";
                    break;
                case "crr":
                case "drk":
                    drawActions.handRight = actionParams[0];
                    drawActions.itemRight = actionParams[0];
                    this._handItem = Number(actionParams[1]);
                    break;
                case "swm":
                    drawActions.swm = "swm";
                    drawActions.geometry = "swhorizontal";
                    if (this._gesture === "spk") {
                        drawActions.speak = "sws";
                    }
                    break;
            }
        }

        if (drawActions.sit === "sit") {
            if (this._direction >= 2 && this._direction <= 4) {
                this._drawOrder = "sit";
                if (drawActions.handRight === "drk" && this._direction >= 2 && this._direction <= 3) {
                    this._drawOrder += ".rh-up";
                } else if (drawActions.handLeft && this._direction === 4) {
                    this._drawOrder += ".lh-up";
                }
            }
        } else if (drawActions.body === "lay") {
            this._drawOrder = "lay";
        } else if (drawActions.handRight === "drk" && this._direction >= 0 && this._direction <= 3) {
            this._drawOrder = "rh-up";
        } else if (drawActions.handLeft && this._direction >= 4 && this._direction <= 6) {
            this._drawOrder = "lh-up";
        }

        this.draw(drawActions);
    }

    private draw(drawActions: HumanDrawAction) {
        type ResolvedPart = { asset: LoadedHumanAsset, type: string, color?: string, drawFlipped: boolean };

        this.drawResult.visualContainer.filters = [];
        this._isGhost = false;

        const activeParts = this._humanManager.getActivePartSets();

        const setParts: any = {};
        for (let partSet of this._figure) {
            const parts = this._humanManager.getPartColor(partSet);
            for (const type in parts) {
                if (setParts[type] == null) {
                    setParts[type] = [];
                }
                setParts[type] = parts[type].concat(setParts[type]);
            }
        }

        if (this._handItem > 0) {
            setParts["ri"] = [{ "index": 0, "id": this._handItem }];
        }

        const resolvedParts: { [key: string]: ResolvedPart[] } = {};


        const drawParts = this._humanManager.getDrawOrder(this._drawOrder, this._direction);


        for (const orderType of drawParts) {
            let drawType = orderType;

            const drawableParts = setParts[drawType];
            if (drawableParts != null) {
                nextPart:
                for (const drawablePart of drawableParts) {
                    let drawDirection = this._direction;
                    let drawAction = null;
                    let partId = drawablePart.id;

                    const packageNames = this._humanManager.getPartPackages(drawType, partId);
                    for (const packageName of packageNames) {
                        const pkg = this._humanLoader.getPackage(packageName);
                        // Are any assets still loading?
                        if (!pkg || pkg.isPending) {
                            this.drawResult.visualContainer.filters = LOADING_FILTERS;
                            this._isGhost = true;
                        }
                    }

                    if (setParts.hidden.includes(drawType)) {
                        continue;
                    }

                    const activeBase = this._headOnly ? "head" : "figure";
                    const activeFigurePart = activeParts[activeBase].activePart;

                    if (this._headOnly && !activeFigurePart.includes(drawType)) {
                        continue;
                    }

                    if (activeParts.figure.activePart.includes(drawType)) {
                        drawAction = drawActions["body"];
                    }
                    if (activeParts.head.activePart.includes(drawType)) {
                        drawDirection = this._headDirection;
                    }
                    if (activeParts.speak.activePart.includes(drawType) && drawActions["speak"]) {
                        drawAction = drawActions["speak"];
                    }

                    if (activeParts.gesture.activePart.includes(drawType) && drawActions["gesture"]) {
                        drawAction = drawActions["gesture"];
                    }
                    if (activeParts.eye.activePart.includes(drawType)) {
                        drawablePart.colorable = false;
                        if (drawActions["eye"]) {
                            drawAction = drawActions["eye"];
                        }
                    }
                    if (activeParts.walk.activePart.includes(drawType) && drawActions["wlk"]) {
                        drawAction = drawActions["wlk"];
                    }
                    if (activeParts.sit.activePart.includes(drawType) && drawActions["sit"]) {
                        drawAction = drawActions["sit"];
                    }
                    if (activeParts.handRight.activePart.includes(drawType) && drawActions["handRight"]) {
                        drawAction = drawActions["handRight"];
                    }
                    if (activeParts.itemRight.activePart.includes(drawType) && drawActions["itemRight"]) {
                        drawAction = drawActions["itemRight"];
                    }
                    if (activeParts.handLeft.activePart.includes(drawType) && drawActions["handLeft"]) {
                        drawAction = drawActions["handLeft"];
                    }
                    if (activeParts.swim.activePart.includes(drawType) && drawActions["swm"]) {
                        drawAction = drawActions["swm"];
                    }

                    if (drawAction == null) {
                        continue;
                    }

                    if (drawType === "hd" && this._worldSize === WorldSize.SMALL) {
                        partId = 1;
                    }

                    if (drawType === "ey" && drawAction === "std" && drawDirection === 3 && partId === 1) {
                        drawAction = "sml";
                    }

                    if (drawType === "fa" && drawAction === "std" && (drawDirection === 2 || drawDirection == 4) && partId === 2) {
                        drawDirection = 1;
                    }

                    const getAsset = (
                        worldSize: WorldSize,
                        action: string,
                        type: string,
                        partId: string | number,
                        direction: string | number,
                        frame: string | number
                    ): LoadedHumanAsset => {
                        const assetName = HumanHelpers.generateResourceName(worldSize, action, type, partId, direction, frame);
                        return this._humanLoader.getLoadedAsset(assetName);
                    };

                    let frameNum = 0;
                    [frameNum, drawAction] = this.getFrameNumber(drawType, drawAction, this._totalFrames);

                    let drawFlipped = false;
                    let asset = getAsset(this._worldSize, drawAction, drawType, partId, drawDirection, frameNum);

                    if (!asset) {
                        const partSet = this._humanManager.getPartSet(drawType);
                        const flippedType = partSet["flipped-set-type"];

                        if (drawDirection > 3 && drawDirection < 7) {
                            drawDirection = <HumanDirection>(6 - drawDirection);

                            if (flippedType) {
                                const tmpAsset = getAsset(this._worldSize, drawAction, flippedType, partId, drawDirection, frameNum);
                                if (tmpAsset) {
                                    asset = tmpAsset;
                                    drawFlipped = true;
                                }
                            }

                            if (!asset) {
                                asset = getAsset(this._worldSize, drawAction, drawType, partId, drawDirection, frameNum);
                                if (asset) {
                                    drawFlipped = true;
                                }
                            }
                        }
                    }

                    if (!asset) {
                        continue;
                    }

                    const color = drawablePart.colorable ? drawablePart.color : null;

                    const resolvedPart: ResolvedPart = {
                        type: drawType,
                        asset,
                        color,
                        drawFlipped
                    }

                    if (resolvedParts[drawType]) {
                        resolvedParts[drawType].push(resolvedPart);
                    } else {
                        resolvedParts[drawType] = [resolvedPart];
                    }
                }
            }
        }

        const geometries = this._humanManager.getCanvasGeometry(HumanHelpers.worldSizeToHumanSize(this._worldSize));
        const geometry = geometries[drawActions.geometry];

        let index = -1;
        for (const drawType of drawParts) {
            let resolvedPartList = resolvedParts[drawType];
            if (resolvedPartList) {
                resolvedPartList = resolvedPartList.reverse();

                for (const resolved of resolvedPartList) {
                    index++;

                    const visualSprite = this._visualSprites[index];
                    const logicSprite = this._logicSprites[index];

                    visualSprite.texture = resolved.asset.texture;
                    logicSprite.texture = resolved.asset.texture;
                    visualSprite.visible = true;
                    logicSprite.visible = true;

                    const imageDim: Dimensions = Dimensions.of(resolved.asset.texture.width, resolved.asset.texture.height);
                    let imageOffsets: Point = Point.of(resolved.asset.assetInfo.offsetX, resolved.asset.assetInfo.offsetY);

                    let posX = -imageOffsets.x;
                    let posY = -imageOffsets.y

                    if (resolved.drawFlipped) {
                        posX = -(posX + imageDim.width) + ((geometry.width / 1.35) - geometry.dx);
                        const flippedSprite = new PIXI.Texture(resolved.asset.texture.baseTexture, undefined, undefined, undefined, PIXI.groupD8.MIRROR_HORIZONTAL);
                        visualSprite.texture = flippedSprite;
                        logicSprite.texture = flippedSprite;

                    }

                    visualSprite.position.set(posX, posY);
                    logicSprite.position.set(posX, posY);


                    if (resolved.color) {
                        visualSprite.tint = ColorUtils.string2hex(resolved.color);
                    } else {
                        visualSprite.tint = 0xFFFFFF;
                    }


                }
            }
        }

        for (let blankLayer = index + 1; blankLayer < MAX_LAYERS; ++blankLayer) {
            this._visualSprites[blankLayer].visible = false;
            this._logicSprites[blankLayer].visible = false;
        }
    }


    private getFrameNumber(part: string, action: string, frame: number): [number, string] {
        const translations: any = { "wav": "Wave", "wlk": "Move", "spk": "Talk" };
        const translation = translations[action];
        if (translation) {
            const animFrames = this._humanManager.getAnimationFrames(translation, part);
            if (animFrames) {
                const count = animFrames.length;
                const actualFrame = animFrames[frame % count];
                if (actualFrame) {
                    return [actualFrame.number, actualFrame.part];
                }
            }
        }
        return [0, action];
    }

    private generateLayers() {
        const colorFilter = new SolidColorFilter(this._logicColor);

        this._visualContainer = new PIXI.Container();
        this._visualSprites = [];
        this._logicContainer = new PIXI.Container();
        this._logicSprites = [];


        for (let i = 0; i < MAX_LAYERS; ++i) {
            const visualSprite = new PIXI.Sprite();
            this._visualContainer.addChild(visualSprite);
            this._visualSprites.push(visualSprite);

            const logicSprite = new PIXI.Sprite();
            logicSprite.filters = [colorFilter];
            this._logicContainer.addChild(logicSprite);
            this._logicSprites.push(logicSprite);
        }

        this._drawResult = {
            visualContainer: this._visualContainer,
            logicContainer: this._logicContainer
        }
    }

    get drawResult(): HumanDrawResult {
        return this._drawResult;
    }

    get isGhost() {
        return this._isGhost;
    }
}

