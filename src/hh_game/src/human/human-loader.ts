import * as PIXI from "pixi.js";
import { HumanAsset, HumanAssetDictionary, StatusPromise, PromiseUtils, FetchCommon, ConvertUtils } from "hh_shared";
import { Game } from "../game";

export type LoadedHumanAsset = { assetInfo: HumanAsset, texture: PIXI.Texture };

export type HumanPackage = {
    assetsDefinition: HumanAssetDictionary;
    assets: { [key: string]: LoadedHumanAsset };
}

export class HumanLoader {
    private _packages = new Map<string, StatusPromise<HumanPackage>>();
    private _loadedAssets = new Map<string, LoadedHumanAsset>();

    getLoadedAsset(assetName: string): LoadedHumanAsset {
        return this._loadedAssets.get(assetName);
    }

    getPackage(packageName: string): StatusPromise<HumanPackage> {
        if (this._packages.has(packageName)) {
            const pkg = this._packages.get(packageName);
            return pkg;
        } else {
            const pkg = PromiseUtils.wrap(this.loadPackage(packageName));
            this._packages.set(packageName, pkg);
            return pkg;
        }
    }

    private async loadPackage(packageName: string): Promise<HumanPackage> {
        const packagePath = `${Game.instance.settings.humanPackagesUrl}/${packageName}.zip`;
       
        const rawPackage = await FetchCommon.fetchPackage(packagePath);

        // Furni Definition
        const rawDef = <string>rawPackage["figurepackage"].content;
        const def = <HumanAssetDictionary>JSON.parse(rawDef);

        const humanPackage: HumanPackage = {
            assetsDefinition: def,
            assets: {}
        }

        // Map existing assets
        for (const assetName in humanPackage.assetsDefinition) {
            const asset = humanPackage.assetsDefinition[assetName];

            if (asset.exists) {
                const packageAsset = rawPackage[asset.name];
                if (packageAsset) {
                    const img = ConvertUtils.convertBufferToImage(<ArrayBuffer>packageAsset.content);
                    const texture = PIXI.Texture.from(img);
                    if (texture) {
                        humanPackage.assets[asset.name] = {
                            assetInfo: asset,
                            texture: texture
                        }
                    }
                }

            }
        }

        // Map alias assets
        for (const assetName in humanPackage.assetsDefinition) {
            const asset = humanPackage.assetsDefinition[assetName];
            if (!asset.exists && asset.source) {
                const source = humanPackage.assets[asset.source];
                if (source) {
                    humanPackage.assets[asset.name] = {
                        assetInfo: asset,
                        texture: source.texture
                    }
                }
            }
        }

        for (const assetName in humanPackage.assets) {
            const content = humanPackage.assets[assetName];
            this._loadedAssets.set(content.assetInfo.name, content);
        }



        return humanPackage;
    }
}