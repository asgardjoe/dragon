import { HumanLoader } from "./human-loader";
import { HumanPartSets, HumanDrawOrder, HumanGeometry, HumanAnimations, FigureMapDictionary, FigureData, FetchCommon, HumanAnimationPartEntry, HumanActivePartSetDictionary, HumanPartSet, HumanDirection, HumanGeometryCanvases } from "hh_shared";
import { HumanPart } from "./human-helpers";
import { Game } from "../game";

export class HumanManager {
    private _loader = new HumanLoader();

    private _humanPartSets: HumanPartSets;
    private _humanDrawOrder: HumanDrawOrder;
    private _humanGeometry: HumanGeometry;
    private _humanAnimation: HumanAnimations;
    private _figureMap: FigureMapDictionary;
    private _figureData: FigureData;

    async start() {
        this._humanPartSets = await FetchCommon.fetchJSON(Game.instance.settings.humanPartSetsUrl);
        this._humanDrawOrder = await FetchCommon.fetchJSON(Game.instance.settings.humanDrawOrderUrl);
        this._humanGeometry = await FetchCommon.fetchJSON(Game.instance.settings.humanGeometryUrl);
        this._humanAnimation = await FetchCommon.fetchJSON(Game.instance.settings.humanAnimationUrl);
        this._figureMap = await FetchCommon.fetchJSON(Game.instance.settings.figureMapUrl);
        this._figureData = await FetchCommon.fetchJSON(Game.instance.settings.figureDataUrl);
    
        for(const packageName of Game.instance.settings.humanPreloadPackages) {
            await this._loader.getPackage(packageName);
        }
    }

    getActivePartSets(): HumanActivePartSetDictionary {
        return this._humanPartSets.activePartSet;
    }

    getPartSet(type: string): HumanPartSet {
        return this._humanPartSets.partSet[type];
    }

    getDrawOrder(action: string, direction: HumanDirection): string[] {
        let drawOrder = this._humanDrawOrder[action][direction];
        if (!drawOrder || drawOrder.length === 0) {
            drawOrder = this._humanDrawOrder["std"][direction];
        }
        return drawOrder;
    }

    getPartPackages(type: string, partId: number): string[] {
        const packages = this._figureMap[type][partId];
        if(packages) return packages;
        return [];
    }

    getCanvasGeometry(size: string): HumanGeometryCanvases {
        return this._humanGeometry.canvas[size];
    }

    getPartColor(humanPart: HumanPart): any {
        const resultParts: any = {};
        const id = parseInt(humanPart.id);
        const partSet = this._figureData.sets[humanPart.type];
        if (partSet != null) {
            if (partSet.set[id] && partSet.set[id].part) {
                const rawParts = partSet.set[id].part;

                for (let rawPart of rawParts) {
                    const element: any = { "index": rawPart.index, "id": rawPart.id, "colorable": rawPart.colorable };

                    if (rawPart.colorable) {
                        element.color = this.getColorByPaletteId(partSet.paletteId, parseInt(humanPart.colors[rawPart.colorIndex - 1]));
                    }

                    if (!resultParts[rawPart.type]) {
                        resultParts[rawPart.type] = [element];
                    } else {
                        resultParts[rawPart.type].push(element);
                    }
                }
            }

            resultParts.hidden = [];
            if (partSet.set[id] && Array.isArray(partSet.set[id].hidden)) {
                for (let partType of partSet.set[id].hidden) {
                    resultParts.hidden.push(partType);
                }
            }
        }
        return resultParts;
    }

    getColorByPaletteId(paletteId: number, colorId: number): string {
        if (this._figureData.colors[paletteId] && this._figureData.colors[paletteId].color && this._figureData.colors[paletteId].color[colorId]) {
            const result = this._figureData.colors[paletteId].color[colorId].colorCode;
            return result;
        }
        return undefined;
    }

    getAnimationFrames(animationName: string, part: string): HumanAnimationPartEntry[] {
        const animData = this._humanAnimation[animationName];
        if(animData) {
            const anims = animData.part[part];
            if(anims) {
                return anims;
            }
        }
    }

    get loader() {
        return this._loader;
    }
}