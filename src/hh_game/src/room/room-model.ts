import { WorldSize, Dimensions } from "hh_shared";

export class RoomModel {
    modelName: string = "";
    worldSize: WorldSize = WorldSize.LARGE;
    floorMap: number[][] = [];
    heightMap: number[][] = [];


    isValidTile(x: number, y: number): boolean {
        if (x < 0 || x >= this.floorMap.length) return false;
        if (y < 0 || y >= this.floorMap[x].length) return false;
        return this.floorMap[x][y] > -1;
    }

    get mapSize(): Dimensions {
        return {
            width: this.floorMap.length,
            height: this.floorMap[0].length
        }
    }
}