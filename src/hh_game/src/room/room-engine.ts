import { RoomFloor } from "./room-floor";
import { RoomModel } from "./room-model";
import { Point, MouseMoveEvent, MouseClickEvent, MouseDoubleClickEvent, InputEvent } from "hh_shared";
import { Game } from "../game";
import { RoomFurni } from "./room-furni";
import { RoomHumans } from "./room-humans";

export class RoomEngine {
    private _floor = new RoomFloor();
    private _furni = new RoomFurni();
    private _humans = new RoomHumans();

    private _model: RoomModel;

    onInput(event: InputEvent) {
        if (this._model) {
            const cameraAdjusted = Point.of(event.position.x -  Game.instance.renderer.logicStage.x, event.position.y - Game.instance.renderer.logicStage.y);

            if (event instanceof MouseMoveEvent) {
                this._floor.updateSelectedTile(cameraAdjusted);
            } else if (event instanceof MouseClickEvent) {
                const selectedColor = Game.instance.renderer.selectColorLogic(event.position);

                if (!this._humans.onClick(selectedColor)) {
                    this._floor.onClick(cameraAdjusted);
                }
            } else if(event instanceof MouseDoubleClickEvent) {
                const selectedColor = Game.instance.renderer.selectColorLogic(event.position);

            }
        }
    }

    applyModel(model: RoomModel) {
        this._model = model;
        this._floor.roomLoaded(model);
    }

    update(delta: number) {
        this._furni.update(delta);
        this._humans.update(delta);
    }

    unload() {
        this._floor.unload();
        this._furni.unload();
        this._humans.unload();
        this._model = undefined;
    }

    get model() {
        return this._model;
    }

    get furni() {
        return this._furni;
    }

    get humans() {
        return this._humans;
    }
}