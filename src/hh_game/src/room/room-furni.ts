import { RoomModel } from "./room-model";
import { FurniInstanceFloor, FurniInstanceFloorOpts } from "../furni/furni-instance-floor";
import { FurniInstanceWall, FurniInstanceWallOpts } from "../furni/furni-instance-wall";
import { WorldSize } from "hh_shared";

type PlacedFloorItem = { id: string, item: FurniInstanceFloor }
type PlacedWallItem = { id: string, item: FurniInstanceWall }


export class RoomFurni {
    private _placedFloorItems: PlacedFloorItem[] = [];
    private _placedWallItems: PlacedWallItem[] = [];


    createFloorItem(opts: FurniInstanceFloorOpts) {
        const floorItem = new FurniInstanceFloor(
            opts
        );

        const placed: PlacedFloorItem = {
            id: opts.id,
            item: floorItem
        }

        this._placedFloorItems.push(placed);
    }

    createWallItem(id: string, opts: FurniInstanceWallOpts) {
        const wallItem = new FurniInstanceWall(
            opts
        );

        const placed: PlacedWallItem = {
            id: id,
            item: wallItem
        }

        this._placedWallItems.push(placed);
    }

    update(delta: number) {
        for (const item of this._placedFloorItems) {
            item.item.update(delta);
        }
    }

    onClick(selectedColor: number): boolean {
        for (const item of this._placedFloorItems) {
            if(item.item.isHitDetected(selectedColor)) return true;
        }
        for (const item of this._placedWallItems) {
            if(item.item.isHitDetected(selectedColor)) return true;
        }
        return false;
    }

    unload() {
        for (const item of this._placedFloorItems) {
            item.item.unload();
        }
        this._placedFloorItems = [];
        for (const item of this._placedWallItems) {
            item.item.unload();
        }
        this._placedWallItems = [];
    }
}