import { RoomEngine } from "./room-engine";
import { ServerMessage, ClientMessages, ServerMessages } from "hh_fuseclient";
import { Game } from "../game";
import { RoomModel } from "./room-model";
import { FurniInstanceFloorOpts } from "../furni/furni-instance-floor";
import { IsometricPoint, FurniDirection, HumanDirection } from "hh_shared";
import { ModelHelpers } from "../model/model-helpers";

export class RoomSystem {
    private _engine = new RoomEngine();
    private _model: RoomModel = new RoomModel();
    private _roomId: string;

    start() {
        Game.instance.client.registerListener(ServerMessage.HOTEL_VIEW, (msg) => this.handleHotelView(msg));
        Game.instance.client.registerListener(ServerMessage.FLAT_LETIN, (_) => this.handleFlatLetin());
        Game.instance.client.registerListener(ServerMessage.ROOM_READY, (msg) => this.handleRoomReady(msg));
        Game.instance.client.registerListener(ServerMessage.HEIGHTMAP, (msg) => this.handleHeightmap(msg));
        Game.instance.client.registerListener(ServerMessage.ACTIVE_OBJECTS, (msg) => this.handleActiveObjects(msg));
        Game.instance.client.registerListener(ServerMessage.USER_OBJECTS, (msg) => this.handleUserObjects(msg));
        Game.instance.client.registerListener(ServerMessage.USER_LOGOUT, (msg) => this.handleUserLogout(msg));
        Game.instance.client.registerListener(ServerMessage.USER_STATUSES, (msg) => this.handleUserStatuses(msg));
    
        Game.instance.events.on("FatalError", () => {
            this.unloadRoom(true);
        });
    }

    tryRoom(roomId: string) {
        this._roomId = roomId;
        Game.instance.client.send(new ClientMessages.TryFlat(roomId));
    }

    unloadRoom(suppressEvent = false) {
        this._engine.unload();

        if(!suppressEvent) Game.instance.events.emit("RoomLeft")
    }

    private handleFlatLetin() {
        this.unloadRoom();
        Game.instance.events.emit("RoomLoading");
        Game.instance.client.send(new ClientMessages.GotoFlat(this._roomId));
    }

    private handleRoomReady(msg: ServerMessages.RoomReady) {
        this._roomId = msg.roomId;
        this._model.modelName = msg.model;
        this._model.floorMap = Game.instance.models.getModelFloormap(msg.model);
        this._model.heightMap = this._model.floorMap;

        this._engine.applyModel(this._model);

        Game.instance.client.send(new ClientMessages.GetHmap());
        //Game.instance.client.send(new ClientMessages.GetUsers());
        Game.instance.client.send(new ClientMessages.GetObjects());
        Game.instance.client.send(new ClientMessages.GetStat());
        //Game.instance.client.send(new ClientMessages.GetItems());

        Game.instance.events.emit("RoomEntered")
    }

    private handleHeightmap(msg: ServerMessages.Heightmap) {
        const processed = ModelHelpers.textMapToArray(msg.heightmap, "\n");
        this._model.heightMap = processed;
    }

    private handleActiveObjects(msg: ServerMessages.ActiveObjects) {
        for(const object of msg.objects) {
            const item: FurniInstanceFloorOpts = {
                id: object.id,
                position: IsometricPoint.of(object.x, object.y, object.z),
                assetName: object.sprite,
                direction: <FurniDirection> object.direction,
                state: 0,
                worldSize: this._model.worldSize
            }
            this._engine.furni.createFloorItem(item);
        }
    }

    private handleUserObjects(msg: ServerMessages.UserObjects) {
        for(const rawHuman of msg.initialUsers) {
            this._engine.humans.createOrUpdateUser({
                instanceId: rawHuman.instanceId,
                userId: rawHuman.userId,
                name: rawHuman.name,
                worldSize: this._model.worldSize,
                figure: rawHuman.figure,
                initialPosition: IsometricPoint.from({x: rawHuman.position.x, y: rawHuman.position.y, z: rawHuman.position.z})
            });
        }
    }

    private handleUserStatuses(msg: ServerMessages.UserStatuses) {
       for(const rawStatus of msg.statuses) {
           this._engine.humans.updateStatus(rawStatus.id, {
               position: IsometricPoint.of(rawStatus.x, rawStatus.y, rawStatus.z),
               direction: <HumanDirection> rawStatus.direction,
               headDirection: <HumanDirection> rawStatus.headDirection,
               actions: rawStatus.actions
           });
       }
    }

    private handleUserLogout(msg: ServerMessages.UserLogout) {
        this._engine.humans.destroyUser(msg.instanceId);
    }

    private handleHotelView(msg: ServerMessages.HotelView) {
        this.unloadRoom();
     }

    get engine() {
        return this._engine;
    }
}