import { Dimensions, WorldSize, IsometricPoint, Point } from "hh_shared";

const COMPARABLE_X_Y = 1000000;
const COMPARABLE_Z = 10000;
const PRIORITY_MULTIPLIER = 10000000;

export namespace WorldConsts {
    export const TILE_MAX_Z = 10;
    export const TILE_STEP = 0.5;

    export const PRIORITIY_FLOOR = 0;
    export const PRIORITIY_FLOOR_SELECT = 1;
    export const PRIORITIY_WALL_ITEM = 2;
    export const PRIORITIY_FLOOR_ITEM = 3;
    export const PRIORITIY_PLAYER = 3;

    export const OFFSET_PLAYER = 1;
}

export namespace WorldHelpers {
    export const getTileDimensions = (worldSize: WorldSize): Dimensions => {
        return Dimensions.of(
            worldSize,
            worldSize / 2
        );
    }

    export const tileToLocal = (worldSize: WorldSize, isoPoint: IsometricPoint): Point => {
        const tileDimensions = WorldHelpers.getTileDimensions(worldSize);
        const tileHH = tileDimensions.height / 2;
        const tileHW = tileDimensions.width / 2;
        const screenX = (isoPoint.x - isoPoint.y) * tileHW;
        const screenY = ((isoPoint.x + isoPoint.y) * tileHH - (isoPoint.z * (tileDimensions.height / 4)));
        return Point.of(screenX, screenY);
    }

    export const localToTile = (worldSize: WorldSize, screenPoint: Point, height: number = 0): IsometricPoint => {
        const tileDimensions = WorldHelpers.getTileDimensions(worldSize);

        const tileHH = tileDimensions.height / 2;
        const tileHW = tileDimensions.width / 2;

        const offsetX = 0;
        const offsetY = height * (tileDimensions.height / 4);

        const xminusy = (screenPoint.x - tileHW - offsetX) / tileHW;
        const xplusy = (screenPoint.y + offsetY) / tileHH;

        const tileX = Math.floor((xminusy + xplusy) / 2);
        const tileY = Math.floor((xplusy - xminusy) / 2);

        return IsometricPoint.of(tileX, tileY, height);
    }

    export const calculateZIndex = (isometricPoint: IsometricPoint, priority: number): number => {
        return ((isometricPoint.x + isometricPoint.y) * (COMPARABLE_X_Y) + (isometricPoint.z * (COMPARABLE_Z))) + PRIORITY_MULTIPLIER * priority;
    };

    export const calculateZIndexFloorItem = (isometricPoint: IsometricPoint, zIndex: number, priority: number, offset: number = 0): number => {
        const compareY = (Math.trunc(zIndex / 100)) / 10;
        const newPoint = IsometricPoint.of(
            isometricPoint.x,
            isometricPoint.y + compareY,
            isometricPoint.z
        );
        return calculateZIndex(newPoint, priority) + offset;
    }

    export const calculateZIndexWallItem = (layerId: number): number => {
        return COMPARABLE_Z + layerId + (PRIORITY_MULTIPLIER * WorldConsts.PRIORITIY_WALL_ITEM);
    };
}