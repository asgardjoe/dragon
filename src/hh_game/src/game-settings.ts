export type GameSettings = {
    furniDataUrl: string,
    furniPackagesUrl: string,

    humanPartSetsUrl: string,
    humanDrawOrderUrl: string,
    humanGeometryUrl: string,
    humanAnimationUrl: string,
    humanPackagesUrl: string,
    humanPreloadPackages: string[],
    figureMapUrl: string,
    figureDataUrl: string
}