export interface GameEvents {
    FatalError: void;
    GameLoading: void;
    GameLoaded: void;
    LoginStart: void;
    LoginSuccess: void;    
    LoginFailed: void;
    RoomLoading: void;
    RoomEntered: void;
    RoomLeft: void;


}