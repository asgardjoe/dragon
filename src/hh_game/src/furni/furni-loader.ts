import { FetchCommon, AssetPackage, ConvertUtils, FurniDataEntry, FurniDataList, FurniCommon } from "hh_shared";
import { FurniBase, LoadedFurniAsset } from "./furni-base";
import * as PIXI from "pixi.js";
import { Game } from "../game";

export class FurniLoader {
    private _furniData = new Map<string, FurniDataEntry>();

    getFurniDataEntry(asset: string) {
        const furniDataEntry = this._furniData.get(asset);
        if (furniDataEntry) {
            return furniDataEntry;
        } else {
            throw new Error(`No furnidata found for "${asset}"`);
        }
    }

    async loadFurniData() {
        const dataPath = Game.instance.settings.furniDataUrl;
        const rawResult = await FetchCommon.fetchJSON<FurniDataList>(dataPath);
        if (rawResult) {
            for (const rawEntry of rawResult) {
                const typedEntry = rawEntry;
                this._furniData.set(typedEntry.asset, typedEntry);
            }
        }
    }

    async getFurniBase(asset: string): Promise<FurniBase> {
        const [itemName] = FurniCommon.splitNameAndColor(asset);

        const packageAssets = await FetchCommon.fetchPackage(`${Game.instance.settings.furniPackagesUrl}/${itemName}.zip`);

        const furniBase = this.createFurniBase(itemName, packageAssets);

        return furniBase;
    }

    private createFurniBase(itemName: string, assetPackage: AssetPackage): FurniBase {
        const furniDef = JSON.parse(<string>assetPackage["furni"].content);

        // Map existing assets
        let assets: { [key: string]: LoadedFurniAsset } = {};
        for (const assetName in furniDef.assets) {
            const asset = furniDef.assets[assetName];
            if (asset.exists) {
                const packageAsset = assetPackage[asset.name];
                if (packageAsset) {
                    const img = ConvertUtils.convertBufferToImage(<ArrayBuffer>packageAsset.content);
                    const texture = PIXI.Texture.from(img);
                    assets[asset.name] = {
                        assetInfo: asset,
                        texture: texture
                    }
                }

            }
        }

        // Map alias assets
        for (const assetName in furniDef.assets) {
            const asset = furniDef.assets[assetName];
            if (!asset.exists && asset.source) {
                const source = assets[asset.source];
                if (source) {
                    assets[asset.name] = {
                        assetInfo: asset,
                        texture: source.texture
                    }
                }
            }
        }

        return {
            itemName: itemName,
            furniDef: furniDef,
            assets: assets
        };
    }

}