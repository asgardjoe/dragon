import { FurniRenderer, FurniRendererOpts, FurniDrawResult } from "./furni-renderer";
import * as PIXI from "pixi.js";
import { FurniType } from "hh_shared";
import { Game } from "../game";

export abstract class FurniInstance extends FurniRenderer {
    private _furniType: FurniType;
    private _initialVisualContainers: PIXI.Container[];
    private _unloaded = false;

    constructor(type: FurniType, opts: FurniRendererOpts) {
        super(opts);
        this._furniType = type;
    }

    protected setup() {
        this.setupPlaceholder();
        this.setupLoad();
    }

    protected drawNextFrame() {
        super.drawNextFrame();
        this.updateScenePosition();
    }

    private setupLoad() {
        this.loadItem().then((success) => {
            if (!this._unloaded && success) {
                Game.instance.renderer.visualStage.removeChild(...this._initialVisualContainers);
                this._initialVisualContainers = undefined;

                Game.instance.renderer.visualStage.addChild(...this.drawResults.map(it => it.visualContainer));
                Game.instance.renderer.logicStage.addChild(...this.drawResults.map(it => it.logicContainer));

                this.updateScenePosition();
            }
        });

    }

    private setupPlaceholder() {
        const placeholderContainer = new PIXI.Container();
        const placeholderSprite = Game.instance.furni.getPlaceholderAsset(this._furniType, this._worldSize, this.direction);
        placeholderContainer.addChild(placeholderSprite);

        const placeholderLayer: FurniDrawResult = {
            visualSprite: placeholderSprite,
            logicSprite: placeholderSprite,
            visualContainer: placeholderContainer,
            logicContainer: placeholderContainer
        }

        this._drawResults = [placeholderLayer];
        this._initialVisualContainers = this.drawResults.map(it => it.visualContainer);
        Game.instance.renderer.visualStage.addChild(...this._initialVisualContainers);

        this.updateScenePosition();
    }

    unload() {
        this._unloaded = true;
        Game.instance.renderer.visualStage.removeChild(...this.drawResults.map(it => it.visualContainer));
        Game.instance.renderer.logicStage.removeChild(...this.drawResults.map(it => it.logicContainer));
    }

    isHitDetected(selectedColor: number): boolean {
        if(selectedColor === this._logicColor) {
            return true;
        }

        return false;
    }

    protected abstract updateScenePosition(): void;

}