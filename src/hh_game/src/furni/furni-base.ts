import { FurniAsset, FurniDefinition } from "hh_shared";

export type LoadedFurniAsset = { 
    assetInfo: FurniAsset, 
    texture: PIXI.Texture
};

export type FurniBase = { 
    furniDef: FurniDefinition;
    itemName: string;
    assets: { 
        [key: string]: LoadedFurniAsset }
}