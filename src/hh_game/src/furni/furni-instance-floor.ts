import { FurniRendererOpts } from "./furni-renderer";
import { FurniInstance } from "./furni-instance";
import { IsometricPoint, FurniType } from "hh_shared";
import { WorldConsts, WorldHelpers } from "../world/world-helpers";

export type FurniInstanceFloorOpts = FurniRendererOpts & {
    position: IsometricPoint
};

export class FurniInstanceFloor extends FurniInstance {
    private _position: IsometricPoint;

    constructor(opts: FurniInstanceFloorOpts) {
        super(FurniType.FLOOR, opts);
        this._position = opts.position;
        this.setup();
    }

    protected updateScenePosition() {
        const tileDimensions = WorldHelpers.getTileDimensions(this._worldSize)
        const local = WorldHelpers.tileToLocal(this._worldSize, <IsometricPoint>this._position);
        const x = local.x + (tileDimensions.width / 2);
        const y = local.y + (tileDimensions.height / 2);

        for (let i = 0; i < this.drawResults.length; ++i) {
            const logicContainer = this.drawResults[i].logicContainer;
            const visualContainer = this.drawResults[i].visualContainer;

            const zIndex = WorldHelpers.calculateZIndexFloorItem(<IsometricPoint>this._position, this.drawResults[i].logicSprite.zIndex, WorldConsts.PRIORITIY_FLOOR_ITEM);

            logicContainer.position.set(x, y);
            logicContainer.zIndex = zIndex;

            visualContainer.position.set(x, y);
            visualContainer.zIndex = zIndex;
        }
    }

    get position() {
        return this._position;
    }

    set position(position: IsometricPoint) {
        this._position = position;
    }
}