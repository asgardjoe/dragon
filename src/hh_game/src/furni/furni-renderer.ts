import { FurniBase, LoadedFurniAsset } from "./furni-base";
import * as PIXI from "pixi.js";
import { FurniStateDictionary, FurniHelpers } from "./furni-helpers";
import { WorldSize, FurniDirection, FurniVisualiazation, FurniCommon } from "hh_shared";
import { SolidColorFilter } from "../renderer/render-filters";
import { Game } from "../game";

const FRAME_SPEED = 120;

export type FurniRendererOpts = {
    worldSize: WorldSize,
    id: string,
    assetName: string,
    direction: FurniDirection,
    state: number;
};

type LayerData = {
    id: number,
    resourceName: string,
    alpha: number,
    frame: number,
    ignoreMouse: boolean,
    z?: number,
    ink?: string,
    color?: number,
    asset?: LoadedFurniAsset
}

export type FurniDrawResults = FurniDrawResult[];

export type FurniDrawResult = {
    visualContainer: PIXI.Container,
    logicContainer: PIXI.Container,
    visualSprite: PIXI.Sprite,
    logicSprite: PIXI.Sprite
}

export class FurniRenderer {
    protected _assetName: string;
    protected _colorId: number;
    protected _worldSize: WorldSize;
    protected _logicColor: number;

    private _base?: FurniBase;
    private _visualization?: FurniVisualiazation;
    private _states?: FurniStateDictionary;

    protected _direction: FurniDirection;
    protected _state: number;

    protected _drawResults: FurniDrawResults;

    private _frame: number = 0;
    private _nextState: number = -1;
    private _frameCounter: number = 0;


    constructor(opts: FurniRendererOpts) {
        this._assetName = opts.assetName;
        this._worldSize = opts.worldSize;
        this._direction = opts.direction;
        this._state = opts.state;

        const [_, colorId] = FurniCommon.splitNameAndColor(this._assetName);
        this._colorId = colorId;

        this._logicColor = Math.floor(Math.random() * (16777215 - 1)) + 1;

        this._drawResults = [];
    }

    async loadItem(): Promise<boolean> {
        try {
            const base = await Game.instance.furni.getFurniBase(this._assetName);
            this.onBaseLoaded(base);
            return true;
        } catch(_) {
            return false;
        }
    }

    update(delta: number) {
        this._frameCounter += delta;
        if (this._frameCounter >= FRAME_SPEED) {
            this.drawNextFrame();
            this._frameCounter = 0;
        }
    }

    protected drawNextFrame() {
        this._frame++;
        if (this._nextState !== -1) {
            if (this._frame >= this._states[this._state].count) {
                this.actuallySetState(this._nextState);
            }
        }
        this.draw();
    }

    private draw() {
        if (this._base) {
            let layerIndex = 0;
            let lastZ = 0;
            for (const layer of this.getLayers(this._direction, this._state, this._frame)) {
                const sprite = this._drawResults[layerIndex].visualSprite;
                const logicSprite = this._drawResults[layerIndex].logicSprite;

                sprite.visible = false;
                logicSprite.visible = false;


                if (layer.asset && layer.asset.texture) {
                    let zIndex: number;
                    if (!layer.z) {
                        zIndex = lastZ + layerIndex;
                    } else {
                        zIndex = (layer.z || 0) + layerIndex;
                    }
                    lastZ = zIndex;

                    sprite.zIndex = zIndex;
                    logicSprite.zIndex = zIndex;

                    sprite.texture = layer.asset.texture;
                    logicSprite.texture = layer.asset.texture;
                    sprite.visible = true;

                    if (!layer.ignoreMouse) {
                        logicSprite.visible = true;
                    }

                    sprite.x = -layer.asset.assetInfo.x;
                    sprite.y = -layer.asset.assetInfo.y;

                    logicSprite.x = -layer.asset.assetInfo.x;
                    logicSprite.y = -layer.asset.assetInfo.y;

                    if (layer.asset.assetInfo.flipH) {
                        sprite.x = layer.asset.assetInfo.x;
                        sprite.scale.x = -1;

                        logicSprite.x = layer.asset.assetInfo.x;
                        logicSprite.scale.x = -1;
                    } else {
                        sprite.scale.x = 1;
                        logicSprite.scale.x = 1;

                    }

                    switch (layer.ink) {
                        case "ADD":
                            sprite.blendMode = PIXI.BLEND_MODES.ADD;
                            break;
                        default:
                            sprite.blendMode = PIXI.BLEND_MODES.NORMAL;
                            break;
                    }

                    if (layer.alpha != null) {
                        sprite.alpha = layer.alpha / 255;
                    } else {
                        sprite.alpha = 1.0;
                    }

                    if (layer.color != null) {
                        sprite.tint = layer.color;
                    } else {
                        sprite.tint = 0xFFFFFF;
                    }

                }

                ++layerIndex;
            }

            for (let i = layerIndex; i < this._drawResults.length; i++) {
                this._drawResults[i].visualSprite.visible = false;
                this._drawResults[i].logicSprite.visible = false;
            }
        }
    }

    private getLayers(direction: FurniDirection, state: number, frame: number) {
        const chunks = [];

        const itemName = this._base.itemName;
        const colorId = this._colorId;

        for (let i = -1; i < this._visualization.layerCount; i++) {
            let layerData: LayerData = { id: i, frame: 0, resourceName: '', alpha: 255, ignoreMouse: false };

            if (i === -1) {
                layerData.alpha = 77;
            }

            if (this._visualization.layers != null) {
                for (let layer of this._visualization.layers) {
                    if (layer.layerId === i) {
                        if (layer.z) {
                            layerData.z = layer.z;
                        }
                        if (layer.ink) {
                            layerData.ink = layer.ink;
                        }
                        if (layer.alpha) {
                            layerData.alpha = layer.alpha;
                        }
                        if (layer.ignoreMouse) {
                            layerData.ignoreMouse = true;
                        }

                    }
                }
            }
            if (this._visualization.directions != null && this._visualization.directions[direction] != null) {
                for (let overrideLayer of this._visualization.directions[direction]) {
                    if (overrideLayer.layerId === i && overrideLayer.z != null) {
                        layerData.z = overrideLayer.z;
                    }
                }
            }

            if (this._visualization.colors != null && this._visualization.colors[colorId] != null) {
                for (let colorLayer of this._visualization.colors[colorId]) {
                    if (colorLayer.layerId === i) {
                        layerData.color = parseInt(colorLayer.color, 16);
                    }
                }
            }

            if (this._visualization.animations != null && this._visualization.animations[state] != null) {
                for (let animationLayer of this._visualization.animations[state].layers) {
                    if (animationLayer.layerId === i && animationLayer.frameSequence != null) {
                        if (animationLayer.frameSequence.length === 1) {
                            layerData.frame = animationLayer.frameSequence[0][frame % animationLayer.frameSequence[0].length];
                        } else {
                            let frameCount = 0;
                            for (let i = 0; i < animationLayer.frameSequence.length; i++) {
                                const currentSequence = animationLayer.frameSequence[i];
                                if (frame < currentSequence.length + frameCount && frame > frameCount) {
                                    layerData.frame = currentSequence[(frame - frameCount - 1) % currentSequence.length];
                                } else {
                                    frameCount += currentSequence.length;
                                }
                            }
                        }
                    }
                }
            }

            layerData.resourceName = FurniHelpers.buildResourceName(itemName, this._worldSize, i, direction, layerData.frame);

            if (this._base.assets[layerData.resourceName]) {
                layerData.asset = this._base.assets[layerData.resourceName];
                chunks.push(layerData);
            }
        }

        return chunks;
    }

    private actuallySetState(state: number) {
        this._state = state;
        this._nextState = -1;
        this._frame = 0;
    }

    private onBaseLoaded(base: FurniBase) {
        this._base = base;

        this._drawResults = [];

        base.furniDef.visualization[this._worldSize];

        this._visualization = base.furniDef.visualization[this._worldSize];
        this._states = FurniHelpers.calculateStates(this._visualization);

        this.generateLayers();

        this.draw();
    }

    private generateLayers() {
        const colorFilter = new SolidColorFilter(this._logicColor);

        for (let layer = 0; layer < this._visualization.layerCount + 1; ++layer) {

            const visualSprite = new PIXI.Sprite();
            const logicSprite = new PIXI.Sprite();
            logicSprite.filters = [colorFilter];

            const visualContainer = new PIXI.Container();
            visualContainer.addChild(visualSprite);

            const logicContainer = new PIXI.Container();
            logicContainer.addChild(logicSprite);

            this._drawResults[layer] = {
                visualContainer,
                visualSprite,
                logicContainer,
                logicSprite
            }
        }
    }

    get state() {
        return this._state;
    }

    set state(state: number) {
        if (this._states && this._states[state] != null) {
            const { transition } = this._states[state];
            if (transition != null) {
                this._state = transition;
                this._frame = 0;
                this._nextState = state;
            } else {
                this.actuallySetState(state);
            }
        }
    }

    get direction() {
        return this._direction;
    }

    set direction(direction: FurniDirection) {
        this._direction = direction;
    }

    get drawResults(): FurniDrawResult[] {
        return this._drawResults;
    }
}