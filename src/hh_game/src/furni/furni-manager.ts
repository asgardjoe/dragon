import { FurniLoader } from "./furni-loader";
import { FurniBase } from "./furni-base";
import * as PIXI from "pixi.js";
import { FurniDataEntry, FurniType, WorldSize } from "hh_shared";

export class FurniManager {
    private _furniLoader = new FurniLoader();

    private _placeholderWall: FurniBase;
    private _placeholderFloor: FurniBase;

    private _furniBases = new Map<string, Promise<FurniBase>>()

    async start() {
        await this._furniLoader.loadFurniData();
        this._placeholderWall = await this.getFurniBase("placeholder_wall");
        this._placeholderFloor = await this.getFurniBase("placeholder_floor");
    }

    getFurniBase(asset: string): Promise<FurniBase> {
        let base = this._furniBases.get(asset);
        if(!base) {
            base = this._furniLoader.getFurniBase(asset);
            this._furniBases.set(asset, base);
        }
        return base;
    }

    getFurniData(asset: string): FurniDataEntry {
        return this._furniLoader.getFurniDataEntry(asset);
    }

    getPlaceholderAsset(furniType: FurniType, worldSize: WorldSize, direction: number): PIXI.Sprite {
        let base: FurniBase;

        switch (furniType) {
            case FurniType.WALL:
                base = this._placeholderWall;
                if (direction !== 2 && direction !== 4) direction = 2;
                break;
            case FurniType.FLOOR:
            default:
                base = this._placeholderFloor;
                direction = 0;
        }

        const textureName = `placeholder_${furniType}_${worldSize}_a_${direction}_0`;
        const asset = base.assets[textureName];
        const sprite = new PIXI.Sprite(asset.texture);
        sprite.x = -asset.assetInfo.x;
        sprite.y = -asset.assetInfo.y;
        return sprite;
    }
}