import { FurniRendererOpts } from "./furni-renderer";
import { Point, FurniType } from "hh_shared";
import { FurniInstance } from "./furni-instance";
import { WorldHelpers } from "../world/world-helpers";

export type FurniInstanceWallOpts = FurniRendererOpts & {
    position: Point
};

export class FurniInstanceWall extends FurniInstance {
    private _position: Point;

    constructor(opts: FurniInstanceWallOpts) {
        super(FurniType.WALL, opts);
        this._position = opts.position;
        this.setup();
    }

    protected updateScenePosition() {
        const x = this._position.x;
        const y = this._position.y;

        for (let i = 0; i < this.drawResults.length; ++i) {
            const logicContainer = this.drawResults[i].logicContainer;
            const visualContainer = this.drawResults[i].visualContainer;

            const zIndex = WorldHelpers.calculateZIndexWallItem(i);

            logicContainer.position.set(x, y);
            logicContainer.zIndex = zIndex;

            visualContainer.position.set(x, y);
            visualContainer.zIndex = zIndex;
        }
    }
}