import { Base64Encoding, VL64Encoding } from "./special-encoding";

export class MessageDeserializer {
    private _body: string;
    private _position = 0;

    constructor(body: string) {
        this._body = body;
    }

    readByte(): string {
        return this._body[this._position++];
    }

    readBytes(length: number): string {
        let result = "";
        for(let i = 0; i < length; ++i) {
            result += this._body[this._position++]
        }
        return result;
    }

    readBase64(length: number = 2): number {
        const values = this.readBytes(length);
        return Base64Encoding.decode(values);
    }

    readString(): string {
        let result = "";
        while(this._body.charCodeAt(this._position) !== 0x02 && !this.atEnd) {
            result += this._body[this._position++];
        }
        this._position++;
        return result;
    }

    readUntil(char: string): string {
        let result = "";
        while(this._body[this._position] !== char&& !this.atEnd) {
            result += this._body[this._position++];
        }
        this._position++;
        return result;
    }

    readLine(): string {
        let result = "";
        while(this._body[this._position] !== "\r" && !this.atEnd) {
            result += this._body[this._position++];
        }
        this._position++;
        return result;
    }

    peekBody(): string {
        let result = "";
        for(let i = this._position; i < this._body.length; ++i) {
            result += this._body[i]
        }
        return result;
    }

    public readInt(): number {
        let remaining = this.peekBody();
        let length = VL64Encoding.calculateLength(remaining);
        let value = VL64Encoding.decode(remaining);
        this.readBytes(length);

        return value;
    }

    get atEnd(): boolean {
        return this._position >= this._body.length;
    }
}