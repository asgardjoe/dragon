export class Base64Encoding {
    public static  encode(i: number, numBytes:number): string {
        let bzRes: string = "";
        for (let j = 1; j <= numBytes; j++)
        {
            let k = ((numBytes - j) * 6);
            bzRes += String.fromCharCode(0x40 + ((i >> k) & 0x3f));
        }

        return bzRes;
    }

    public static decode(bzData: string): number {
        let i = 0;
        let j = 0;
        for (let k = bzData.length - 1; k >= 0; k--)
        {
            let x = bzData.charCodeAt(k) - 0x40;
            if (j > 0)
                x *= Math.pow(64, j);
            i += x;
            j++;
        }
        return i;
    }
}

export class VL64Encoding {
    static MAX_INTEGER_BYTE_AMOUNT = 6;

    static encode(i: number): string {
        let wf: string[] = new Array(this.MAX_INTEGER_BYTE_AMOUNT);

        let pos = 0;
        let numBytes = 1;
        let startPos = pos;
        let negativeMask = i >= 0 ? 0 : 4;

        i = Math.abs(i);

        wf[pos++] = String.fromCharCode(64 + (i & 3));

        for (i >>= 2; i != 0; i >>= VL64Encoding.MAX_INTEGER_BYTE_AMOUNT)
        {
            numBytes++;
            wf[pos++] = String.fromCharCode(64 + (i & 0x3f));
        }
        wf[startPos] = String.fromCharCode(wf[startPos].charCodeAt(0) | numBytes << 3 | negativeMask);

        let result = "";
        for(let i =0; i < numBytes; ++i) {
            result += wf[i];
        }
        
        return result;
    }

    static decode(bzData: string): number {
        let pos = 0;
        let v = 0;

        let negative = (bzData.charCodeAt(pos) & 4) == 4;
        let totalBytes = bzData.charCodeAt(pos) >> 3 & 7;

        v = bzData.charCodeAt(pos) & 3;

        pos++;

        let shiftAmount = 2;

        for (let b = 1; b < totalBytes; b++)
        {
            v |= (bzData.charCodeAt(pos) & 0x3f) << shiftAmount;
            shiftAmount = 2 + 6 * b;
            pos++;
        }

        if (negative) {
            v *= -1;
        }

        return v;
    }

    static calculateLength(bzData: string): number {
        return bzData.charCodeAt(0) >> 3 & 7;
    }
}