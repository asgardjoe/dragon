import { FuseClient } from "./fuse-client";

export class ClientSocket {
    private _fuseClient: FuseClient;
    private _connected: boolean;
    private _ws?: WebSocket;

    constructor(fuseClient: FuseClient) {
        this._fuseClient = fuseClient;
    }

    connect(connectionURL: string): Promise<void> {
        return new Promise((resolve, reject) => {
            this._ws = new WebSocket(connectionURL);

            this._ws.onopen = evt => {
                this._connected = true;
                this._fuseClient.onConnected();
                resolve();
            };

            this._ws.onclose = evt => {
                this._connected = false;
                this._fuseClient.onDisconnected();
            };

            this._ws.onmessage = evt => {
                this._fuseClient.onMessagePart(evt.data);
            };

            this._ws.onerror = evt => {
                if (!this._connected) {
                    reject('Cannot connect to host');
                }
                this._connected = false;
                this._fuseClient.onDisconnected();
            };
        });
    }

    disconnect() {
        if(this._connected) {
            this._connected = false;
            this._ws.close();
        }
    }

    send(msg: string) {
        if(this._connected) {
            this._ws.send(msg);
        }
    }

    get connected() {
        return this._connected;
    }
}