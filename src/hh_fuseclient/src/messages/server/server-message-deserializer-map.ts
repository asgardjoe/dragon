import { MessageDeserializer } from "../../util/message-deserializer";
import { ServerMessage } from "./server-message";
import { Hello } from "./handshake/hello";
import { Ping } from "./misc/ping";
import { LocalisedError } from "./misc/localised-error";
import { LoginOK } from "./handshake/login-ok";
import { FuseRights } from "./user/fuse-rights";
import { UserObject } from "./user/user-object";
import { CreditBalance } from "./user/credit-balance";
import { AvailableSets } from "./user/available-sets";
import { AvailableBadges } from "./user/available-badges";
import { RoomReady } from "./room/room-ready";
import { FlatLetIn } from "./room/flat-let-in";
import { ActiveObjects } from "./room/active-objects";
import { Heightmap } from "./room/heightmap";
import { UserObjects } from "./room/user-objects";
import { UserStatuses } from "./room/user-statuses";
import { UserLogout } from "./room/user-logout";
import { HotelView } from "./room/hotel-view";

export class ServerMessageDeserializerMap {
    private _map: Map<number, (deserializer: MessageDeserializer) => ServerMessage>;

    constructor() {
        this._map = new Map<number, (deserializer: MessageDeserializer) => ServerMessage>();
        
        this._map.set(ServerMessage.HELLO, (parser) => new Hello(parser));
        this._map.set(ServerMessage.FUSE_RIGHTS, (parser) => new FuseRights(parser));
        this._map.set(ServerMessage.LOGIN_OK, (parser) => new LoginOK(parser));
        this._map.set(ServerMessage.HOTEL_VIEW, (parser) => new HotelView(parser));
        this._map.set(ServerMessage.USER_OBJECT, (parser) => new UserObject(parser));
        this._map.set(ServerMessage.CREDIT_BALANCE, (parser) => new CreditBalance(parser));
        this._map.set(ServerMessage.AVAILABLE_SETS, (parser) => new AvailableSets(parser));
        this._map.set(ServerMessage.PING, (parser) => new Ping(parser));
        this._map.set(ServerMessage.LOCALISED_ERROR, (parser) => new LocalisedError(parser));
        this._map.set(ServerMessage.FLAT_LETIN, (parser) => new FlatLetIn(parser));
        this._map.set(ServerMessage.ROOM_READY, (parser) => new RoomReady(parser));
        this._map.set(ServerMessage.HEIGHTMAP, (parser) => new Heightmap(parser));
        this._map.set(ServerMessage.ACTIVE_OBJECTS, (parser) => new ActiveObjects(parser));
        this._map.set(ServerMessage.USER_OBJECTS, (parser) => new UserObjects(parser));
        this._map.set(ServerMessage.USER_LOGOUT, (parser) => new UserLogout(parser));
        this._map.set(ServerMessage.USER_STATUSES, (parser) => new UserStatuses(parser));
        this._map.set(ServerMessage.AVAILABLE_BADGES, (parser) => new AvailableBadges(parser));
    }

    doDecoding(deserializer: MessageDeserializer): ServerMessage {
        const header = deserializer.readBase64(2);
        const mapper = this._map.get(header);
        if(mapper) {
            const msg =  mapper(deserializer);
            msg.decode();
            return msg;
        }
    }
}