import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class Ping extends ServerMessage {
    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.PING, deserializer);
    }

    decode() {
        
    }
}