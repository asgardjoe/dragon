import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class LoginOK extends ServerMessage {
    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.LOGIN_OK, deserializer);
    }

    decode() {
        
    }
}