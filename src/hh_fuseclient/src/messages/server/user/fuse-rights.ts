import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class FuseRights extends ServerMessage {
    rights: string[];

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.FUSE_RIGHTS, deserializer);
    }

    decode() {
        let result: string[] = [];
        while(!this._deserializer.atEnd) {
            const right = this._deserializer.readString();
            result.push(right);
        }
        this.rights = result;
    }
}