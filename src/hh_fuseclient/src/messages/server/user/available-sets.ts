import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class AvailableSets extends ServerMessage {
    sets: number[];

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.AVAILABLE_SETS, deserializer);
    }

    decode() {
        const body = this._deserializer.peekBody();
        this.sets = eval(body);
    }
}