import { ServerMessage } from "../server-message"
import { MessageDeserializer } from "../../../util/message-deserializer";

export class CreditBalance extends ServerMessage {
    credits: number;

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.CREDIT_BALANCE, deserializer);
    }

    decode() {
        this.credits = parseInt(this._deserializer.peekBody());
    }
}