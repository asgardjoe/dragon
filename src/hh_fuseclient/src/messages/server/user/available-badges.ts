import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class AvailableBadges extends ServerMessage {
    badges: string[] = [];
    visible: boolean = false;
    selectedIndex: number = 0;

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.AVAILABLE_BADGES, deserializer);
    }

    decode() {
        const count = this._deserializer.readInt();
        for(let i = 0; i < count; ++i) {
            const badgeCode = this._deserializer.readString();
            this.badges.push(badgeCode);
        }
        this.visible = this._deserializer.readInt() !== 0;
        this.selectedIndex = this._deserializer.readInt() - 1;
    }
}