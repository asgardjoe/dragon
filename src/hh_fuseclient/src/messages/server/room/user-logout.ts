import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class UserLogout extends ServerMessage {
    instanceId: string;

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.USER_LOGOUT, deserializer);
    }

    decode() {
        this.instanceId = this._deserializer.peekBody();
    }
}