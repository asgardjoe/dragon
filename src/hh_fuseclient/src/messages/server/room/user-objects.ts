import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";
import { ServerMessages } from "../../..";

export type UserObjectEntry = {
    instanceId: string,
    userId: string,
    name: string,
    figure: string,
    badge: string,
    position: { x: number, y: number, z: number }
}

export class UserObjects extends ServerMessage {
    initialUsers: UserObjectEntry[] = [];


    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.USER_OBJECTS, deserializer);
    }

    decode() {
        const userMaps: Map<string, string>[] = [];

        const body = this._deserializer.peekBody();
        const users = body.split("\r\r");
        for (const user of users) {
            const values = new Map<string, string>();
            for (const line of user.split("\r")) {
                if (line == "") continue;
                const [key, value] = line.split(":");
                values.set(key, value);
            }
            userMaps.push(values);
        }

        for (const user of userMaps) {
            const posSplit = user.get("l").split(" ");
            const x = parseInt(posSplit[0]);
            const y = parseInt(posSplit[1]);
            const z = parseFloat(posSplit[2]);

            let endUser: UserObjectEntry = {
                instanceId: user.get("i"),
                userId: user.get("a"),
                name: user.get("n"),
                figure: user.get("f"),
                badge: user.get("b"),
                position: {
                    x,
                    y,
                    z
                }
            }

            this.initialUsers.push(endUser);
        }
    }
}