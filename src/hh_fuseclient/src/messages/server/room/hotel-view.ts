import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export class HotelView extends ServerMessage {
    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.HOTEL_VIEW, deserializer);
    }

    decode() {
        
    }
}