import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export type UserStatusEntry = {
    id: string,
    x: number,
    y: number,
    z: number,
    direction: number,
    headDirection: number,
    actions: {[key: string]: string[]}
}

export class UserStatuses extends ServerMessage {
        constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.USER_STATUSES, deserializer);
    }
    statuses: UserStatusEntry[] = [];

    decode() {
        
        do {
            let actions: {[key: string]: string[]} = {};

            const id = this._deserializer.readUntil(" ");
            const positionRaw = this._deserializer.readUntil("/");
            const [x, y, z, headDir, bodyDir] = positionRaw.split(",");
            const restOfLine = this._deserializer.readLine();
            if(restOfLine) {
                const rawActions = restOfLine.split("/");
                for(const rawAction of rawActions) {
                    const [action, actionProps] = rawAction.split(" ");
                    actions[action] = [actionProps];
                }
            }

            const status: UserStatusEntry = {
                id: id,
                x: parseInt(x),
                y: parseInt(y),
                z: parseFloat(z),
                direction: parseInt(bodyDir),
                headDirection: parseInt(headDir),
                actions: actions
            };

            this.statuses.push(status);
        }while(!this._deserializer.atEnd);

    }
}