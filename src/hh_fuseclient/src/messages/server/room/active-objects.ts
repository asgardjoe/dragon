import { ServerMessage } from "../server-message";
import { MessageDeserializer } from "../../../util/message-deserializer";

export type ActiveObjectEntry = {
    id: string,
    sprite: string,
    x: number, 
    y: number,
    z: number,    
    direction: number,
    length: number,
    width: number,
    color: string,
    behavior: number,
    customData: string
}

export class ActiveObjects extends ServerMessage {
    objects: ActiveObjectEntry[] = [];

    constructor(deserializer: MessageDeserializer) {
        super(ServerMessage.ACTIVE_OBJECTS, deserializer);
    }

    decode() {
        const numItems = this._deserializer.readInt();
        for(let i = 0; i < numItems; ++i) {
            const id = this._deserializer.readString();
            const sprite = this._deserializer.readString();
            const x = this._deserializer.readInt();
            const y = this._deserializer.readInt();
            const length = this._deserializer.readInt();
            const width = this._deserializer.readInt();
            const direction = this._deserializer.readInt();
            const z = parseFloat(this._deserializer.readString())
            const color = this._deserializer.readString();
            this._deserializer.readString(); // Unknown
            const behavior = this._deserializer.readInt()
            const customData = this._deserializer.readString();

            const floorItem: ActiveObjectEntry = {
                id, sprite, x, y, z, direction, length, width, color, behavior, customData
            }

            this.objects.push(floorItem);
        }
    }
}