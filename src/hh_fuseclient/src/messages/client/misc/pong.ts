import { ClientMessage } from "../client-message";

export class Pong extends ClientMessage {
    constructor() {
        super(ClientMessage.PONG);
    }

    encode() {
       
    }
}