import { ClientMessage } from "../client-message";

export class TryFlat extends ClientMessage {
    roomId: string;
    password?: string;

    constructor(roomId?: string, password?: string) {
        super(ClientMessage.TRYFLAT);
        this.roomId = roomId;
        this.password = password;
    }

    encode() {
        this._serializer.writeRaw(this.roomId);
        if(this.password) {
            this._serializer.writeRaw("/");
            this._serializer.writeRaw(this.password)
        }
    }
}