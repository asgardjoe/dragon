import { ClientMessage } from "../client-message";

export class Walk extends ClientMessage {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        super(ClientMessage.WALK);
        this.x = x;
        this.y = y;
    }

    encode() {
        this._serializer.writeBase64(this.x, 2);
        this._serializer.writeBase64(this.y, 2);
    }
}