import { ClientMessage } from "../client-message";

export class TryLoginMessage extends ClientMessage {
    username: string;
    password: string;

    constructor(username?: string, password?: string) {
        super(ClientMessage.TRY_LOGIN);
        this.username = username;
        this.password = password;
    }

    encode() {
        this._serializer.writeString(this.username);
        this._serializer.writeString(this.password);
    }
}