import { ClientMessage } from  "../client-message";

export class Register extends ClientMessage {
    username: string;
    password: string;
    figure: string;
    email: string;
    gender: string;
    birthday: string;


    constructor(
        username?: string,
        password?: string,
        figure?: string,
        email?: string,
        gender?: string,
        birthday?: string,
    ) {
        super(ClientMessage.REGISTER);
        this.username = username;
        this.password = password;
        this.figure = figure;
        this.email = email;
        this.gender = gender;
        this.birthday = birthday;
    }

    encode() {
        this._serializer.writeBase64(0, 2);
        this._serializer.writeString(this.username);
        this._serializer.writeBase64(0, 2);
        this._serializer.writeString(this.figure);
        this._serializer.writeBase64(0, 2);
        this._serializer.writeString(this.gender);
        this._serializer.writeBase64(0, 6);
        this._serializer.writeString(this.email);
        this._serializer.writeBase64(0, 2);
        this._serializer.writeString(this.birthday);
        this._serializer.writeBase64(0, 11);
        this._serializer.writeString(this.password);
    }
}