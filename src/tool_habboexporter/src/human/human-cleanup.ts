import { injectable, inject } from "inversify";
import { Settings } from "../settings";
import mkdirp from "mkdirp";
import fs from "fs";
import { FigureData, FigureMapDictionary } from "hh_shared";

@injectable()
export class HumanCleanup {
    @inject(Settings)
    private settings: Settings;

    process() {
        if(this.settings.cleanupHuman) {
            this.doCleanup();
        }
    }

    doCleanup() {
        const figureData = <FigureData> JSON.parse(fs.readFileSync(`${this.settings.humanCleanupInput}/figuredata.json`, "utf-8"));
        const figureMap = <FigureMapDictionary> JSON.parse(fs.readFileSync(`${this.settings.humanCleanupInput}/figuremap.json`, "utf-8"));

        for(const setType in figureData.sets) {
            const setParent = figureData.sets[setType];
            for(const setId in setParent.set) {
                const set = setParent.set[setId];
                for(const part of set.part) {
                    if(!figureMap[part.type] || !figureMap[part.type][part.id]) {
                        // Invalid
                        figureData.sets[setType].set[setId] = undefined;
                    }
                }
            }
        }

        const outputPath = `${this.settings.humanCleanupOutput}`;
        mkdirp.sync(outputPath);
        fs.writeFileSync(`${outputPath}/figuredata.json`, Buffer.from(JSON.stringify(figureData, undefined, 2)));


    }
}