import { injectable, inject } from "inversify";
import { Settings } from "../settings";
import { FurniDataEntry, FurniType, FurniDataList, FurniDataFloorType, FurniDataWallType, FetchCommon } from "hh_shared";
import mkdirp from "mkdirp";
import path from "path";
import fs from "fs";
import { XMLUtils } from "../utils/xml-utils";

@injectable()
export class FurniDataSystem {
    @inject(Settings)
    private settings: Settings;

    async process() {
        if (this.settings.downloadFurniData) {
            await this.download();
        }

        if (this.settings.convertFurniData) {
            await this.convert();
        }
    }

    private async download() {
        ("Downloading furni data...");
        const xml = await FetchCommon.fetchText(this.settings.furniDataUrl);
        const file = this.settings.furniDataDownloadPath;
        const dir = path.dirname(file);
        mkdirp.sync(dir);
        fs.writeFileSync(file, Buffer.from(xml));
        console.log("Downloaded furni data.");
    }

    private async convert() {
        console.log("Converting furni data...");

        const inputFile = this.settings.furniDataDownloadPath;
        if (!fs.existsSync(inputFile)) {
            throw new Error(`Can not convert furnidata because '${inputFile}' does not exist.`);
        }

        const xml = fs.readFileSync(inputFile, "utf-8");

        const parsed = XMLUtils.parseXml(xml);
        const floorItems = XMLUtils.parseXmlArray(parsed.furnidata.roomitemtypes.furnitype);
        const wallItems = XMLUtils.parseXmlArray(parsed.furnidata.wallitemtypes.furnitype);

        const items: FurniDataList = [];

        for (const item of floorItems) {
            const typeData: FurniDataFloorType = {
                dimensions: {
                    x: item.xdim,
                    y: item.ydim,
                    z: 0
                },
                defaultDir: item.defaultdir
            };

            const newItem: FurniDataEntry = {
                asset: item.classname,
                name: item.name,
                description: item.description,
                revision: item.revision,
                type: FurniType.FLOOR,
                typeData: typeData
            };

            items.push(newItem);
        }

        for (const item of wallItems) {
            const typeData: FurniDataWallType = {};

            const newItem: FurniDataEntry = {
                asset: item.classname,
                name: item.name,
                description: item.description,
                revision: item.revision,
                type: FurniType.WALL,
                typeData: typeData
            };

            items.push(newItem);
        }

        const file = this.settings.furniDataExportPath;
        const dir = path.dirname(file);
        mkdirp.sync(dir);
        fs.writeFileSync(file, Buffer.from(JSON.stringify(items, null, 2)));

        console.log("Converted furni data.");

    }
}