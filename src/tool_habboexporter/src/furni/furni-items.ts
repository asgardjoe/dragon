import { injectable, inject } from "inversify";
import { Settings } from "../settings";
import mkdirp from "mkdirp";
import path from "path";
import fs from "fs";
import { SWFUtils } from "../utils/swf-utils";
import { XMLUtils } from "../utils/xml-utils";
import JSZip from "jszip";
import { FurniDataList, FurniCommon, FurniDefinition, FurniLogic, FurniDirection, FurniAssetDictionary, FurniVisualiazationDictionary, FurniVisualiazation, FetchCommon } from "hh_shared";

@injectable()
export class FurniItemsSystem {
    @inject(Settings)
    private settings: Settings;

    async process() {
        if (this.settings.downloadFurniItems) {
            await this.download();
        }

        if (this.settings.extractFurniItems) {
            await this.extract();
        }

        if (this.settings.convertFurniItems) {
            await this.convert();
        }

        if (this.settings.packageFurniItems) {
            await this.package();
        }
    }

    private async download() {
        console.log("Downloading furni items...");

        const furniDataPath = this.settings.furniDataExportPath;
        const furniItemsOutputPath = this.settings.furniItemsDownloadPath;
        const furniItemsUrl = this.settings.furniItemsUrl;

        if (!fs.existsSync(furniDataPath)) {
            throw new Error(`Could not download furni items because furni data file '${furniDataPath}' does not exist.`);
        }

        const furniDataRaw = fs.readFileSync(furniDataPath, "utf-8");
        const furniDatas: FurniDataList = JSON.parse(furniDataRaw);

        mkdirp.sync(furniItemsOutputPath);

        const itemNames: string[] = [];

        for (const furniData of furniDatas) {
            const [itemName] = FurniCommon.splitNameAndColor(furniData.asset);
            if (itemNames.includes(itemName)) continue;
            itemNames.push(itemName);

            console.log(`Downloading furni item ${itemName}...`);

            const url = `${furniItemsUrl}/${furniData.revision}/${itemName}.swf`;
            try {
                const data = await FetchCommon.fetchBinary(url);
                fs.writeFileSync(`${furniItemsOutputPath}/${itemName}.swf`, Buffer.from(data));
                console.log(`Downloaded furni item ${itemName}.`);
            } catch (e) {
                console.log(`Could not download ${itemName}.`);
            }
        }

        console.log("Downloaded furni items.");
    }

    private async extract() {
        console.log("Extracting furni items...");

        const swfsPath = this.settings.furniItemsDownloadPath;
        const outputPathPrefix = this.settings.furniItemsExtractPath;

        if (!fs.existsSync(swfsPath)) {
            throw new Error(`Could not extract furni items because furni items path '${swfsPath}' does not exist.`);
        }

        const swfs = fs.readdirSync(swfsPath).filter(fn => fn.endsWith(".swf"));

        for (const swfFileName of swfs) {
            const swfPath = `${swfsPath}/${swfFileName}`;
            const swfName = swfFileName.substring(0, swfFileName.length - 4);
            console.log(`Extracting furni item ${swfName}...`);
            const outputPath = `${outputPathPrefix}/${swfName}`;
            mkdirp.sync(outputPath);
            await SWFUtils.extractSwf(swfPath, outputPath, swfName.length + 1);
            console.log(`Extracted furni item ${swfName}.`);

        }

        console.log("Extracted furni items.");
    }

    private async convert() {
        console.log("Converting furni items...");

        const extractedPath = this.settings.furniItemsExtractPath;

        if (!fs.existsSync(extractedPath)) {
            throw new Error(`Could not extract furni items because furni items path '${extractedPath}' does not exist.`);
        }

        const extractedItems = fs.readdirSync(extractedPath).filter(name => !name.startsWith("."));

        for (const item of extractedItems) {
            this.convertItem(extractedPath, item);
        }

        console.log("Converted furni items.");
    }

    private async convertItem(extractedPath: string, itemName: string) {
        console.log(`Converting item ${itemName}...`);

        const inputPath = `${extractedPath}/${itemName}`;
        const outputPath = `${this.settings.furniItemsConvertPath}/${itemName}`;
        mkdirp.sync(outputPath);
        const files = fs.readdirSync(inputPath).filter(fn => !fn.startsWith("."));

        let indexFile: string;
        let visualizationFile: string;
        let assetsFile: string;
        let logicFile: string;

        const allFiles: string[] = [];
        const imageFiles: string[] = [];


        for (const file of files) {
            allFiles.push(file);

            if (file.endsWith(".png")) {
                imageFiles.push(file);
            } else if (file.includes("visualization.xml")) {
                visualizationFile = file;
            } else if (file.includes("index.xml")) {
                indexFile = file;
            } else if (file.includes("assets.xml")) {
                assetsFile = file;
            } else if (file.includes("logic.xml")) {
                logicFile = file;
            }
        }

        if (!visualizationFile || !assetsFile || !logicFile || !indexFile) {
            throw new Error(`Could not convert ${itemName} as a required file was missing`);
        }

        const visualizationXml = fs.readFileSync(`${inputPath}/${visualizationFile}`, "utf-8");
        const assetsXml = fs.readFileSync(`${inputPath}/${assetsFile}`, "utf-8");
        const logicXml = fs.readFileSync(`${inputPath}/${logicFile}`, "utf-8");
        const indexXml = fs.readFileSync(`${inputPath}/${indexFile}`, "utf-8");


        const logic = this.generateLogicFromXml(logicXml, indexXml);
        const assets = this.generateAssetsFromXml(assetsXml, imageFiles);
        const visualization = this.generateVisualizationFromXml(visualizationXml);

        const furniDef: FurniDefinition = {
            assets,
            logic,
            visualization
        };

        fs.writeFileSync(`${outputPath}/furni.json`, Buffer.from(JSON.stringify(furniDef, undefined, 2)));

        for (const image of imageFiles) {
            fs.copyFileSync(`${inputPath}/${image}`, `${outputPath}/${image}`);
        }

        console.log(`Converted item ${itemName}.`);
    }

    private generateLogicFromXml(logicXml: string, indexXml: string): FurniLogic {
        let logicParsed = XMLUtils.parseXml(logicXml);
        let indexParsed = XMLUtils.parseXml(indexXml);

        if (logicParsed != null && indexParsed != null) {
            let rawLogic = logicParsed.objectData;
            let rawIndex = indexParsed.object;
            let newDirections: FurniDirection[] = [];
            if (rawLogic.model.directions != null) {
                let rawDirections = XMLUtils.parseXmlArray(rawLogic.model.directions.direction);
                for (const rawDir of rawDirections) {
                    newDirections.push(rawDir.id);
                }
            }

            let logic: FurniLogic = {
                behavior: {
                    logic: rawIndex.logic,
                    visual: rawIndex.visualization
                },
                dimensions: {
                    x: rawLogic.model.dimensions.x,
                    y: rawLogic.model.dimensions.y,
                    z: rawLogic.model.dimensions.z,
                },
                directions: newDirections
            };
            return logic;
        }
        return null;
    };

    private generateAssetsFromXml(rawXml: string, imageFiles: string[]): FurniAssetDictionary {
        const parsed = XMLUtils.parseXml(rawXml);
        const assetDictionary: FurniAssetDictionary = {};
        const assets = XMLUtils.parseXmlArray(parsed.assets.asset);
        for (const rawAsset of assets) {
            const mappedFile = imageFiles.find((name) => name.startsWith(rawAsset.name));
            let data = {
                name: rawAsset.name,
                x: rawAsset.x,
                y: rawAsset.y,
                exists: !!mappedFile,
                flipH: !!rawAsset.flipH,
                source: rawAsset.source
            };
            assetDictionary[data.name] = data;
        }
        return assetDictionary;
    };

    private generateVisualizationFromXml(rawXml: string): FurniVisualiazationDictionary {
        const parsed = XMLUtils.parseXml(rawXml);
        const rawVisualization: any = parsed.visualizationData.graphics != null ? XMLUtils.parseXmlArray(parsed.visualizationData.graphics.visualization) : XMLUtils.parseXmlArray(parsed.visualizationData.visualization);
        const parsedVisualizations = rawVisualization.map(function (rawVisualizationData: any) {
            const visualizationData: FurniVisualiazation = {
                angle: rawVisualizationData.angle,
                layerCount: rawVisualizationData.layerCount,
                size: rawVisualizationData.size,
                directions: {},
            };
            if (rawVisualizationData.directions != null && rawVisualizationData.directions.direction != null) {
                const rawDirections = XMLUtils.parseXmlArray(rawVisualizationData.directions.direction);
                rawDirections.forEach(function (direction) {
                    visualizationData.directions[direction.id] = [];
                });
            }
            if (rawVisualizationData.colors != null && rawVisualizationData.colors.color != null) {
                const rawColors = XMLUtils.parseXmlArray(rawVisualizationData.colors.color);
                const allegedColors1: any = {};
                for (const color of rawColors) {
                    allegedColors1[color.id] = [];
                    let colorLayers = XMLUtils.parseXmlArray(color.colorLayer);
                    for (const rawColorLayer of colorLayers) {
                        allegedColors1[color.id].push({
                            layerId: rawColorLayer.id,
                            color: rawColorLayer.color,
                        });
                    }
                }
                visualizationData.colors = allegedColors1;
            }
            if (rawVisualizationData.animations != null && rawVisualizationData.animations.animation != null) {
                const rawAnimations = XMLUtils.parseXmlArray(rawVisualizationData.animations.animation);
                const allegedAnimations1: any = {};
                rawAnimations.forEach(function (animation) {
                    const animationLayers = XMLUtils.parseXmlArray(animation.animationLayer);
                    const layers: any = [];
                    animationLayers.forEach(function (animationLayer) {
                        const rawFrameSequences = XMLUtils.parseXmlArray(animationLayer.frameSequence);
                        const frameSequence = rawFrameSequences.map(function (rawFrameSequence) {
                            const actualSequences = XMLUtils.parseXmlArray(rawFrameSequence.frame);
                            return actualSequences.map(function (layer) {
                                return layer.id;
                            });
                        });
                        layers.push({
                            layerId: animationLayer.id,
                            frameSequence: frameSequence,
                        });
                    });
                    allegedAnimations1[animation.id] = {
                        id: animation.id,
                        layers: layers,
                    };
                });
                visualizationData.animations = allegedAnimations1;
            }
            if (rawVisualizationData.layers != null && rawVisualizationData.layers.layer != null) {
                const rawLayers = XMLUtils.parseXmlArray(rawVisualizationData.layers.layer);
                const newLayers: any = [];
                rawLayers.forEach(function (rawLayer) {
                    const newLayer = {
                        layerId: rawLayer.id,
                        ignoreMouse: !!rawLayer.ignoreMouse,
                        ink: rawLayer.ink,
                        alpha: rawLayer.alpha,
                        z: rawLayer.z
                    };
                    newLayers.push(newLayer);
                });
                visualizationData.layers = newLayers;
            }
            return visualizationData;
        });
        const icon = parsedVisualizations.find(function (value: any) { return value.size === 1; });
        const small = parsedVisualizations.find(function (value: any) { return value.size === 32; });
        const large = parsedVisualizations.find(function (value: any) { return value.size === 64; });
        if (icon != null && large != null) {
            return {
                1: icon,
                32: small,
                64: large,
            };
        }
    }

    private async package() {
        console.log("Packaging furni items...");

        const convertedPath = this.settings.furniItemsConvertPath;
        const outputPath = this.settings.furniItemsPackagesPath;
        mkdirp.sync(outputPath);

        if (!fs.existsSync(convertedPath)) {
            throw new Error(`Could not package items because '${convertedPath}' does not exist.`);
        }

        const folders = fs.readdirSync(convertedPath).filter(name => !name.startsWith("."));

        for(const folder of folders) {
            const srcPath = `${convertedPath}/${folder}`;
            if(fs.statSync(srcPath).isDirectory()) {
                console.log(`Packaging furni item ${folder}...`);
                const fileContents = fs.readdirSync(srcPath);
                const files = fileContents.filter((val) => !val.startsWith(".") && !val.startsWith("__"));
                const zip = new JSZip();
                for(const file of files) {
                    const fileContents = fs.readFileSync(`${srcPath}/${file}`);
                    zip.file(file, fileContents);
                }

                const arrayBuffer = await zip.generateAsync({type: "arraybuffer" });
                fs.writeFileSync(`${outputPath}/${folder}.zip`, Buffer.from(arrayBuffer));
                console.log(`Packaged furni item ${folder}.`);

            }
        }

        console.log("Packaged furni items.");

    }
}