import fs from "fs";
import { Settings } from "./settings";
import { Container } from "inversify";
import { FurniDataSystem } from "./furni/furni-data";
import { FurniItemsSystem } from "./furni/furni-items";
import { GenericExtractor } from "./generic/generic-extractor";
import { HumanPackage } from "./human/human-package";
import { HumanCleanup } from "./human/human-cleanup";

export class App {
    private container = new Container();

    async run() {
        console.log("⚒ Habbo Exporter ⚒");

        const settingsRaw = fs.readFileSync("./settings.json", "utf-8");
        const settings: Settings = JSON.parse(settingsRaw);

        this.container.bind(App).toConstantValue(this);
        this.container.bind(Container).toConstantValue(this.container);
        this.container.bind(Settings).toConstantValue(settings);

        this.container.bind(FurniDataSystem).toSelf().inSingletonScope();
        this.container.bind(FurniItemsSystem).toSelf().inSingletonScope();

        this.container.bind(HumanPackage).toSelf().inSingletonScope();
        this.container.bind(HumanCleanup).toSelf().inSingletonScope();

        this.container.bind(GenericExtractor).toSelf().inSingletonScope();

        await this.container.get(FurniDataSystem).process();
        await this.container.get(FurniItemsSystem).process();
        await this.container.get(HumanPackage).process();
        await this.container.get(HumanCleanup).process();

        await this.container.get(GenericExtractor).process();

    }
}
