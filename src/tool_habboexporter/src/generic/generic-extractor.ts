import { injectable, inject } from "inversify";
import { Settings } from "../settings";
import mkdirp from "mkdirp";
import path from "path";
import fs from "fs";
import { SWFUtils } from "../utils/swf-utils";

@injectable()
export class GenericExtractor {
    @inject(Settings)
    private settings: Settings;

    async process() {
        if(this.settings.extractGeneric) {
            await this.extractGeneric();
        }
    }

    async extractGeneric() {
        const inputPath = this.settings.extractGenericInput;

        if (!fs.existsSync(inputPath)) {
            throw new Error(`Could not extract generic '${inputPath}' does not exist.`);
        }


        const swfs: string[] = [];

        if(fs.statSync(inputPath).isFile() && inputPath.endsWith(".swf")) {
            swfs.push(inputPath);
        } else if (fs.statSync(inputPath).isDirectory()) {
            const files = fs.readdirSync(inputPath).filter(name => name.endsWith(".swf"));
            swfs.push(...files);
        }

        for(const swf of swfs) {
            const outputFolder = path.join(this.settings.extractGenericOutput, `${swf}_out`);
            mkdirp.sync(outputFolder);
            const swfPath = path.join(inputPath, swf);
            SWFUtils.extractSwf(swfPath,outputFolder);
        }
    }
}