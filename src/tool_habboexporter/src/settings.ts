import { injectable } from "inversify";

export class Settings {
    extractGeneric: boolean;
    downloadFurniData: boolean;
    convertFurniData: boolean
    convertFurniItems: boolean;
    downloadFurniItems: boolean;
    extractFurniItems: boolean;
    packageFurniItems: boolean;
    extractHuman: boolean;
    convertHuman: boolean;
    packageHuman: boolean;
    cleanupHuman: boolean;

    furniDataUrl: string;
    furniDataDownloadPath: string;
    furniDataExportPath: string;
    furniItemsUrl: string;
    furniItemsDownloadPath: string;
    furniItemsExtractPath: string;
    furniItemsConvertPath: string;
    furniItemsPackagesPath: string;

    humanDownloadPath: string;
    humanExtractPath: string;
    humanConvertPath: string;
    humanPackagesPath: string;
    humanCleanupInput: string;
    humanCleanupOutput: string;

    extractGenericInput: string;
    extractGenericOutput: string;
}